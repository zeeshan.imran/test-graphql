## 1. Changes:
## 2. Common structure
```typescript
{
  "id": "5e22e5fdcab6a20c28960416",
  "stats": {
    "survey_name": "GoGurt",
    "tabs": [
      {
        "title": "Demographics",
        "panels": [
          {
            "title": "Gender (%)",
            ...otherQuestionInfo
            "question_type": "choose-one",
            "question_id": "5e22e5ffe87e9c0c06a5b83d",
            "charts": [
              {
                "chart_type": "pie", // pie | column | polar | polars | spider | map | table | avg-table | std-table | pca-3d | stacked-column-horizontal-bars...
                "labels": [ 
                    { "name": "Female", "analytics_value": 1 },
                    { "name": "Male", "analytics_value": 2 },
                    { "name": "Other", "analytics_value": 3 }
                ],
                "products": [
                    { "id": "from-mongo", "name": "Berry / Strawberry (8 count)" },
                    { "id": "from-mongo", "name": "Berry / Strawberry (8 count)" }
                ],
                "status": "loading", //loading,completed,error
                "message": "error message", // if status === error
                //,...other meta properties of chart: range, 
                "statistics": <<Statistics>>,
                "data": <<ChartData>>
              } as ChartMetaData & { data: ChartData } & { statistics: Statistics },
              ...otherCharts
            ]
          },
          ...otherPanels
        ]
      },
      // ...otherTabs
    ]
  }
}

type ChartMetaData = {
  chart_type: "pie" | "column" | "polar" | "spider" | "map" | "table",
  labels: Array<{ name: string, analytics_value: number}>,
  products: Array<{ id: string, name: string }>,
  status: "loading" | "completed" | "error",
  range?: [Number, Number]
}
type ChartData = Array<ColumnDataEntry | PolarDataEntry | TableDataEntry>
type Statistics = {
    avg?: AverageStatistic
}
```
## 3. Chart data:
### 3.1. Column | Pie | Line | Map:
```typescript
type ColumnDataEntry = {
    analytics_value: number
    label: string
    value: number
}
// example
[
  {
    analytics_value: 1,
    label: "Berry / Strawberry (8 count)",
    value: 35
  },
  {
    analytics_value: 1,
    label: "Berry / Strawberry (8 count)",
    value: 35
  },
  ...otherEntries
]
```

### 3.2 Polar:
```typescript
type PolarDataEntry = number
// example
[3.9739, 3.6242, 5, 4.9671, 6.5333, 4.3624, 5.786, 5.7566]
```

### 3.3 Table:
```typescript
type Row = Array<any>
type TableDataEntry = Row
// example
[
    ["Berry / Strawberry (8 count)", 20],
    ["Berry / Strawberry (8 count)", 30],
]
```

## 4. Statistic Data:
### 4.1 Average:
```typescript
type AverageStatistic = {
  labels: Array<string>,
  data: Array<AverageDataEntry>
}
type AverageDataEntry = {
    name: string,
    value: number
}
// example
{
  labels: ["Product", "Average"],
  data: [
    {
        label: "Andros Gourmand & Végétal Le Délice Chocolat"
        value: 8.00952380952381
    },
    {
        label: "Andros Gourmand & Végétal Le Délice Chocolat"
        value: 8.00952380952381
    }
  ]
}
```
