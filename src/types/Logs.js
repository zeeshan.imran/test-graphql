const Logs = /* GraphQL */ `
  type Logs {
    id: ID!
    action: String
    type: String
    values: JSON
    collections: [String]
    by: User
    impersonatedBy: User
    relatedSurvey: Survey
    relatedEnrollment: SurveyEnrollment
    relatedQuestion: Question
    relatedProduct: Product
    relatedUser: User
    relatedOrganization: Organization
    relatedAnswer: Answer
    createdAt: Date
    updatedAt: Date
  }

  input CountSurveyLogsInput {
    keyword: String
  }

  enum OrderField {
    createdAt
  }

  input SurveyLogsInput {
    keyword: String
    #
    skip: Int
    limit: Int
    orderBy: OrderField
    orderDirection: OrderDirection
  }

  extend type Query {
    surveyLogs(surveyId: ID!, input: SurveyLogsInput): [Logs]
    countSurveyLogs(surveyId: ID!, input: CountSurveyLogsInput): Int
    photoValidationLogs(surveyEnrollmentId: ID!): [Logs]
  }
`

module.exports = Logs
