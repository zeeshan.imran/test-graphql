const Survey = /* GraphQL */ `
  type CustomButtons {
    continue: String
    start: String
    next: String
    skip: String
  }

  type AutoAdvanceSettings {
    active: Boolean
    debounce: String
    hideNextButton: Boolean
  }

  type PdfFooterSettings {
    active: Boolean
    footerNote: String
  }

  type ScreenOutSettings {
    reject: Boolean
    rejectAfterStep: Int
  }

  type PromotionSettings {
    active: Boolean
    showAfterTaster: Boolean
    code: String
  }
  type PromotionProduct {
    label: String
    url: String
  }
  type PromotionOptions {
    promoProducts: [PromotionProduct]
  }
  type SurveyProductRewardsRuleType {
    min: Float
    max: Float
    percentage: Float
    active: Boolean
  }

  enum ProductDisplayTypes {
    reverse
    permutation
    forced
    none
  }

  type Survey {
    id: ID!
    name: String
    title: String
    coverPhoto: String # url for picture
    creator: User
    lastModifier: User
    uniqueName: String
    state: String
    minimumProducts: Int
    maximumProducts: Int
    products: [Product]
    instructionsText: JSON
    instructionSteps: [String]
    screeningQuestions: [Question]
    setupQuestions: [Question]
    productsQuestions: [Question]
    finishingQuestions: [Question]
    paymentQuestions: [Question]
    thankYouText: JSON
    rejectionText: JSON
    screeningText: JSON
    customizeSharingMessage: JSON
    loginText: JSON
    pauseText: JSON
    authorizationType: String
    maximumReward: Int
    settings: JSON
    enabledEmailTypes: [String!]!
    emails: Emails
    surveyLanguage: String
    customButtons: CustomButtons
    exclusiveTasters: [User]
    sharedStatsUsers: [User]
    allowRetakes: Boolean
    isScreenerOnly: Boolean
    showSurveyProductScreen: Boolean
    showIncentives: Boolean
    productDisplayType: ProductDisplayTypes
    showGeneratePdf: Boolean
    forcedAccount: Boolean
    forcedAccountLocation: String
    tastingNotes: TastingNotes
    country: String
    availableIn: String
    referralAmount: Int
    isPaypalSelected: Boolean
    isGiftCardSelected: Boolean
    isScreenerShownInShare: Boolean
    isCompulsorySurveyShownInShare: Boolean
    autoAdvanceSettings: AutoAdvanceSettings
    pdfFooterSettings: PdfFooterSettings
    promotionSettings: PromotionSettings
    promotionOptions : PromotionOptions
    sharingButtons: Boolean
    showSharingLink: Boolean
    validatedData: Boolean
    compulsorySurvey: Boolean
    showSurveyScore: Boolean
    showInternalNameInReports: Boolean
    reduceRewardInTasting: Boolean
    includeCompulsorySurveyDataInStats: Boolean 
    showInPreferedLanguage: Boolean
    addDelayToSelectNextProductAndNextQuestion: Boolean
    showOnTasterDashboard: Boolean
    stats(
      productIds: [String!]
      questionFilters: [Object]
      dateFilter: [Date]
      sync: Boolean
      enrollmentId: [String]
    ): JSON
    savedRewards: JSON
    maxProductStatCount: Int
    owner: String
    allowedDaysToFillTheTasting: Int
    linkedSurveys: [Survey]
    screeners: [Survey!]!
    compulsory: [Survey!]!
    createdAt: Float
    updatedAt: Float
    openedAt: Float
    screenOut: Boolean
    screenOutSettings: ScreenOutSettings
    productRewardsRule: SurveyProductRewardsRuleType
    showUserProfileDemographics: Boolean
    folder: String
    dualCurrency: Boolean
    referralAmountInDollar: Int
    retakeAfter: Int
    maxRetake: Int
  }

  type SurveyShares {
    id: ID
    fullName: String
    email: String
    amount: String
    currency: String
    referral: ID
    refferedIds: [ID]
    amountInDollar: String
  }

  type Emails {
    surveyWaiting: Email
    surveyRejected: Email
    surveyCompleted: Email
  }

  type TastingNotes {
    tastingId: String
    tastingLeader: String
    customer: String
    country: String
    dateOfTasting: String
    otherInfo: String
  }

  type Email {
    subject: String
    html: String
    text: String
  }

  enum SurveyState {
    draft
    active
    suspended
    deprecated
  }

  enum SurveyOrderField {
    name
  }

  input SurveysInput {
    keyword: String
    state: SurveyState
    parentId: ID
    country: String
    #
    orderBy: SurveyOrderField
    orderDirection: OrderDirection
    skip: Int
    limit: Int
    omitScreeners: Boolean
    omitUsedTastings: ID
    authorizationType: String
    maximumReward: Int
    showAllSurveys: Boolean
    onlyShared: Boolean
  }

  input CountSurveysInput {
    keyword: String
    state: SurveyState
    parentId: ID
    country: String
    onlyShared: Boolean
  }

  type QuestionInfoWithOrder {
    id: ID
    displayOn: String
    typeOfQuestion: String
  }

  extend type Query {
    isUniqueNameDuplicated(uniqueName: String, surveyId: ID): Boolean
    survey(id: ID, checkOrg: Boolean): Survey
    surveys(input: SurveysInput!): [Survey]
    quickAccessSurveys: [Survey]
    surveysTotal(input: CountSurveysInput!): Int
    allSurveys: [Survey]
    getQuestionChartData(survey: ID, user: ID, questions: [ID]): JSON
    getDemoSurvey(demoName: String): Survey
    surveysPdf(
      id: ID
      productIds: [String]
      questionFilters: [Object]
      isImpersonating: Boolean
    ): String
    surveysPpt(
      surveyId: ID!
      jobGroupId: ID!
      currentEnv: String!
    ): String
    getStatsResults(jobGroupId: ID!, jobIds: [ID!]): JSON
    surveysTasterPdf(id: ID, surveyEnrollment: String): String
    compulsorySurveyCountry: Survey
    getQuestionIdWithOrder(surveyId: ID!): [QuestionInfoWithOrder]
  }

  input SurveyProductRewardsRuleInput {
    min: Float
    max: Float
    percentage: Float
    active: Boolean
  }

  input CreateSurveyBasicsInput {
    name: String
    coverPhoto: String # url for picture
    title: String
    owner: ID!
    predecessorSurvey: ID
    products: [ID]
    state: String
    instructionsText: JSON
    instructionSteps: [String]
    thankYouText: JSON
    rejectionText: JSON
    screeningText: JSON
    customizeSharingMessage: JSON
    loginText: JSON
    pauseText: JSON
    uniqueName: String
    authorizationType: String
    maximumReward: Int
    exclusiveTasters: [String]
    allowRetakes: Boolean
    isScreenerOnly: Boolean
    showSurveyProductScreen: Boolean
    showIncentives: Boolean
    productDisplayType: ProductDisplayTypes
    showGeneratePdf: Boolean
    forcedAccount: Boolean
    forcedAccountLocation: String
    tastingNotes: JSON
    referralAmount: Int
    isPaypalSelected: Boolean
    isGiftCardSelected: Boolean
    recaptcha: Boolean
    minimumProducts: Int
    maximumProducts: Int
    surveyLanguage: String
    country: String
    customButtons: JSON
    savedRewards: JSON
    maxProductStatCount: Int
    autoAdvanceSettings: JSON
    pdfFooterSettings: JSON
    promotionSettings: JSON
    promotionOptions : JSON
    sharingButtons: Boolean
    showSharingLink: Boolean
    validatedData: Boolean
    compulsorySurvey: Boolean
    showSurveyScore: Boolean
    showInternalNameInReports: Boolean
    reduceRewardInTasting: Boolean
    includeCompulsorySurveyDataInStats: Boolean
    showInPreferedLanguage: Boolean
    addDelayToSelectNextProductAndNextQuestion: Boolean
    showOnTasterDashboard: Boolean
    allowedDaysToFillTheTasting: Int
    linkedSurveys: [ID]
    enabledEmailTypes: [String!]!
    emails: JSON
    screenOut: Boolean
    screenOutSettings: JSON
    productRewardsRule: SurveyProductRewardsRuleInput
    showUserProfileDemographics: Boolean
    dualCurrency: Boolean
    referralAmountInDollar: Int
    retakeAfter: Int
    maxRetake: Int
  }

  input CreateSurveyInput {
    basics: CreateSurveyBasicsInput
    products: [ReplaceProductInput!]
    questions: [ReplaceQuestionInput!]
  }

  type CreateSurveyPayload {
    survey: Survey
    replacedProducts: [ReplaceProductPayload!]!
    replacedQuestions: [ReplaceQuestionPayload!]!
  }

  input UpdateSurveyBasicsInput {
    id: ID
    name: String
    title: String
    coverPhoto: String # url for picture
    instructionsText: JSON
    instructionSteps: [String]
    thankYouText: JSON
    rejectionText: JSON
    screeningText: JSON
    customizeSharingMessage: JSON
    loginText: JSON
    pauseText: JSON
    uniqueName: String
    authorizationType: String
    maximumReward: Int
    exclusiveTasters: [String]
    allowRetakes: Boolean
    isScreenerOnly: Boolean
    showSurveyProductScreen: Boolean
    showIncentives: Boolean
    productDisplayType: ProductDisplayTypes
    showGeneratePdf: Boolean
    forcedAccount: Boolean
    forcedAccountLocation: String
    tastingNotes: JSON
    referralAmount: Int
    isPaypalSelected: Boolean
    isGiftCardSelected: Boolean
    recaptcha: Boolean
    minimumProducts: Int
    maximumProducts: Int
    surveyLanguage: String
    country: String
    customButtons: JSON
    maxProductStatCount: Int
    autoAdvanceSettings: JSON
    pdfFooterSettings: JSON
    promotionSettings: JSON
    promotionOptions : JSON
    sharingButtons: Boolean
    showSharingLink: Boolean
    validatedData: Boolean
    compulsorySurvey: Boolean
    showSurveyScore: Boolean
    showInternalNameInReports: Boolean
    reduceRewardInTasting: Boolean
    includeCompulsorySurveyDataInStats: Boolean
    showInPreferedLanguage: Boolean
    addDelayToSelectNextProductAndNextQuestion: Boolean
    showOnTasterDashboard: Boolean
    allowedDaysToFillTheTasting: Int
    linkedSurveys: [ID]
    enabledEmailTypes: [String!]!
    emails: JSON
    screenOut: Boolean
    screenOutSettings: JSON
    productRewardsRule: SurveyProductRewardsRuleInput
    showUserProfileDemographics: Boolean
    dualCurrency: Boolean
    referralAmountInDollar: Int
    retakeAfter: Int
    maxRetake: Int
  }

  input UpdateSurveyInput {
    isRemoveDataFromDraft: Boolean
    basics: UpdateSurveyBasicsInput
    products: [ReplaceProductInput!]
    questions: [ReplaceQuestionInput!]
  }

  type UpdateSurveyPayload {
    survey: Survey
    replacedProducts: [ReplaceProductPayload!]!
    replacedQuestions: [ReplaceQuestionPayload!]!
  }

  input UpdateSurveyProductsInput {
    id: ID
    products: [ID]
  }

  enum TemplateName {
    operator
    taster
  }

  extend type Mutation {
    setOpenedAt(id: ID): Boolean
    createSurvey(input: CreateSurveyInput!): CreateSurveyPayload
    updateSurvey(input: UpdateSurveyInput!): UpdateSurveyPayload
    cloneSurvey(id: ID!): ID
    deprecateSurvey(id: ID!): ID
    suspendSurvey(id: ID!): Survey
    resumeSurvey(id: ID!): Survey
    updateSurveyBasics(input: UpdateSurveyBasicsInput): Survey
    updateSurveyProducts(input: UpdateSurveyProductsInput): Survey
    getSurveyShares(surveyId: ID!): [SurveyShares]
    getGiftCardShares(surveyId: ID!): [SurveyShares]
    getProductIncentives(surveyId: ID!): [SurveyShares]
    getGiftCardIncentives(surveyId: ID!): [SurveyShares]
    shareSurveyStatsScreen(
      surveyId: ID
      users: [String]
      isScreenerShownInShare: Boolean
      isCompulsorySurveyShownInShare: Boolean
    ): Survey
    sendSurveyReport(
      surveyId: ID!
      userId: ID!
      screener: ID
      dateFilter: [Date]
      includeCompulsorySurvey: Boolean
      reportList: [String!]!
    ): Boolean
    sendSurveyPhotos(surveyId: ID!, userId: ID!): Boolean
    getDownloadLink(
      surveyId: ID!
      jobGroupId: ID!
      templateName: TemplateName!
      layout: JSON
    ): String
  }
`

module.exports = Survey
