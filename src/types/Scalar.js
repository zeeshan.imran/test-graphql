const Scalar = /* GraphQL */ `
  scalar JSON
  scalar Date
`

module.exports = Scalar
