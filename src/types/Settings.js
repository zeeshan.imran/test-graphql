const Brand = /* GraphQL */ `
  extend type Query {
    getChartsSettings(surveyId: ID): JSON
  }

  extend type Mutation {
    saveChartsSettings(
      surveyId: ID
      chartsSettings: JSON
      operatorPdfLayout: JSON
      tasterPdfLayout: JSON
    ): JSON
  }
`

module.exports = Brand
