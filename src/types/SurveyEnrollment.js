const SurveyEnrollment = /* GraphQL */ `
  type SurveyEnrollment {
    id: ID!
    survey: Survey
    state: String
    lastAnsweredQuestion: Question
    user: User
    selectedProducts: [Product]
    paypalEmail: String
    referral: ID
    savedRewards: JSON
    savedQuestions: JSON
    processed: Boolean
    hiddenFromCharts: Boolean
    validation: String
    comment: String
    escalation: Boolean
    browserInfo: JSON
    productDisplayOrder: [String]
    productDisplayType: String
    socioEconomicRating: JSON
    enrollmentReferralAmount: Int
    rejectedEnrollment: Boolean #Rejected Enrollment addition
    rejectedQuestion: [String]
    allowedDaysToFillTheTasting: Int
    createdAt: Float
    updatedAt: Float
    expiredAt: Float
    enrollmentReferralAmountInDollar: Int
  }

  type SavedRewards {
    id: ID
    answered: Boolean
    reward: Float
    showOnCharts: Boolean
    payable: Boolean
    delayToNextProduct: String
    extraDelayToNextProduct: String
    startedAt: Int
  }

  input BlackListUser {
    id: ID
    emailAddress: String
    paypalEmailAddress: String
    fullName: String
    blackListed: Boolean
  }

  input SavedRewardsInput {
    id: ID
    answered: Boolean
    reward: Float
    showOnCharts: Boolean
    payable: Boolean
    delayToNextProduct: String
    extraDelayToNextProduct: String
    startedAt: Float
  }

  input EditValidateSurveyEnrollement {
    id: ID!
    surveyId: String
    processed: Boolean
    hiddenFromCharts: Boolean
    validation: String
    comment: String
    escalation: Boolean
    user: BlackListUser
    savedRewards: [SavedRewardsInput]
  }

  type Funnel {
    responses: Int
    product: [FunnelProduct]
    question: Question
    userAnswer: Int
  }

  type SurveyFunnel {
    total: Int
    funnel: [Funnel]
  }

  input SubmitForcedSignUpInput {
    surveyEnrollment: ID!
    email: String!
    password: String
    country: String
  }

  input SubmitAnswerInput {
    question: ID!
    surveyEnrollment: ID!
    context: JSON
    value: JSON
    startedAt: String
    selectedProduct: ID
    rejectedEnrollment: Boolean #rejected enrollment addition
    rejectedQuestion:[String]
  }

  input TastingProfileDataInput {
    enrollment: ID
    survey: ID
    product: ID
    question: ID
    labels: [String]
    profileName: String
  }

  type ChartSeries {
    name: String
    data: [Float]
  }

  type TastingProfileData {
    labels: [String]
    series: [ChartSeries]
  }

  type SliderProfileData {
    labels: [String]
    series: [ChartSeries]
  }

  type UpdatedRecordCount {
    updatedRecords: Int
  }

  type FunnelEnrollment {
    enrollment: ID
    product: String
    question: String
    userInfo: [Object]
    value: [Object]
    enrollmentInfo: SurveyEnrollment
  }

  type ValidateSurveyEnrollmentFunnel {
    funnelAnswer: [FunnelEnrollment]
  }

  type SurveyEnrollmentValidation {
    id: ID!
    paypalEmail: String
    processed: Boolean
    hiddenFromCharts: Boolean
    validation: String
    comment: String
    escalation: Boolean
    user: User
    answers: [Answer]
    savedRewards: [SavedRewards]
  }

  input SurveyEnrollmentValidationParams {
    status: String
    productId: ID
    userId: ID
    keyword: String
    skip: Int
    limit: Int
  }

  input CountSurveyEnrollmentValidationParams {
    status: String
    productId: ID
    userId: ID
    keyword: String
  }

  extend type Query {
    surveyEnrollment(id: ID): SurveyEnrollment
    surveyEnrollments: [SurveyEnrollment]
    tastingProfileData(input: TastingProfileDataInput!): TastingProfileData
    sliderProfileData(input: TastingProfileDataInput!): TastingProfileData
    surveyEnrollmentValidations(surveyId: ID!, params: SurveyEnrollmentValidationParams): [SurveyEnrollmentValidation]
    countSurveyEnrollmentValidations(surveyId: ID!, params: CountSurveyEnrollmentValidationParams): Int
    surveyFunnel(surveyId: ID, currentPage: Int, perPage: Int): SurveyFunnel
    userSurveyEnrollments(userId: ID!): [SurveyEnrollment]
    surveyFunnelValidation(questionId: ID , productId: ID): ValidateSurveyEnrollmentFunnel
  }

  input EnrollmentStateInput {
    surveyEnrollment: String
  }

  input UpdateEnrollmentExpiry {
    enrollmentId: ID!
    days: Int!
  }

  extend type Mutation {
    saveRewards(rewards: JSON, surveyEnrollment: ID): Boolean
    saveQuestions(questions: JSON, surveyEnrollment: ID): Boolean
    submitAnswer(input: SubmitAnswerInput!): Boolean
    submitForcedSignUp(input: SubmitForcedSignUpInput!): Boolean
    setSelectedProducts(
      surveyEnrollment: ID!
      selectedProducts: [ID!]
    ): SurveyEnrollment
    setPaypalEmail(
      surveyEnrollment: ID!
      paypalEmail: String!
    ): SurveyEnrollment
    finishSurvey(input: EnrollmentStateInput): SurveyEnrollment
    rejectSurvey(input: EnrollmentStateInput): Boolean
    finishSurveyScreening(input: EnrollmentStateInput): Boolean
    updateValidateSurveyEnrollement(
      input: EditValidateSurveyEnrollement
    ): SurveyEnrollment
    updateSurveyEnrolmentProductIncentiveStatus(
      surveyID: ID
      enrollments: [ID]
    ): UpdatedRecordCount
    updateSurveyEnrolmentGiftCardIncentiveStatus(
      surveyID: ID
      enrollments: [ID]
    ): UpdatedRecordCount
    updateSurveyShareStatus(surveyID: ID, enrollments: [ID]): UpdatedRecordCount
    updateUserEnrollementExpiry(input: UpdateEnrollmentExpiry): SurveyEnrollment
  }
`

module.exports = SurveyEnrollment
