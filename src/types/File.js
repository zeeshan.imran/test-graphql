const File = /* GraphQL */ `
  type File {
    type: String!
    fileName: String!
  }

  type FileInfo {
    id: ID!
    file: File!
    size: Float!
  }

  type FileDownloadLink {
    id: ID!
    file: File!
    link: String!
  }

  extend type Query {
    getFileInfo(id: ID!): FileInfo!
    getFileDownloadLink(id: ID!): FileDownloadLink!
  }
`

module.exports = File
