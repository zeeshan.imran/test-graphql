const Folder = /* GraphQL */ `
  type Children {
    id: ID!
    name: String!
    owner: ID
    parent: ID
    status: String
    createdAt: Float
    updatedAt: Float
  }

  type FolderPath {
    pathName: [String]
    pathId: [ID]
  }

  type Folder {
    id: ID!
    name: String!
    owner: ID
    parent: ID
    children: [Children]
    surveys: [ID]
    status: String
    createdAt: Float
    updatedAt: Float
  }

  extend type Query {
    getFolderPath(folderId: ID): FolderPath
    showFolder(parentId: ID, state: String, keyword: String): JSON
    listFolders(parentId: ID): [Folder]
  }

  extend type Mutation {
    createFolder(name: String!, parent: ID): Folder
    updateFolder(name: String!, parent: ID, id: ID!): Folder
    destroyFolder(id: ID!): Folder
    moveSurveyToFolder(id: ID, folderId: ID, surveyId: ID): Folder
  }
`

module.exports = Folder
