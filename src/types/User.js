const User = /* GraphQL */ `
  type User {
    id: ID!
    emailAddress: String
    fullName: String
    birthYear: Int
    nationality: String
    gender: String
    surveyEnrollments: [SurveyEnrollment]
    organization: Organization
    initialized: Boolean
    type: String
    exclusiveSurveys: [Survey]
    isSuperAdmin: Boolean
    isTaster: Boolean
    isVerified: Boolean
    dateofbirth: String
    country: String
    state: String
    language: String
    ethnicity: String
    incomeRange: String
    hasChildren: String
    numberOfChildren: String
    childrenAges: String
    smokerKind: String
    foodAllergies: String
    marketResearchParticipation: String
    paypalEmailAddress: String
    isProfileCompleted: Boolean
    blackListed: Boolean
    compulsorySurveyId: ID
    isCompulsorySurveyTaken: Boolean
    isDummyTaster: Boolean
  }

  type Validation {
    validCount: Int
    invalidCount: Int
    validAnswers: [Answer]
    invalidAnswers: [Answer]
  }

  type AuthPayload {
    token: String # JSON Web Token
    user: User
  }

  enum UserOrderField {
    fullName
    emailAddress
    type
    organization
  }

  input UsersInput {
    keyword: String
    type: [String!]
    #
    orderBy: UserOrderField
    orderDirection: OrderDirection
    skip: Int
    limit: Int
    inactive: Boolean
  }

  input CountUsersInput {
    keyword: String
    type: [String!]
    inactive: Boolean
  }
  
  input isResetPasswordTokenValidInput {
    email: String
    token: String
  }

  extend type Query {
    user(id: ID): User
    me(impersonate: Boolean): User
    users(input: UsersInput!): [User]
    countUsers(input: CountUsersInput!): Int
    superAdminUsersByOrganization: [User]
    isResetPasswordTokenValid(input: isResetPasswordTokenValidInput): Boolean
    userValidations(userId: ID!): Validation
    paypalUsers(paypalEmailAddress: String!): [User]
  }

  input RequestAccountInput {
    email: String
    userType: String
    firstName: String
    lastName: String
    companyName: String
    phoneNumber: String
  }

  input CreateTasterAccountInput {
    emailAddress: String
    password: String
    type: String
    country: String
    state: String
    language: String
    isDummyTaster: Boolean
    referral: String
    source: String
  }

  input UpdateTasterAccountInput {
    id: ID
    firstName: String
    lastName: String
    dateofbirth: String
    gender: String
    ethnicity: String
    incomeRange: String
    hasChildren: String
    numberOfChildren: String
    childrenAges: String
    smokerKind: String
    foodAllergies: String
    marketResearchParticipation: String
    paypalEmailAddress: String
    isProfileCompleted: Boolean
  }
  input verifiedEmail {
    email: String
    password: String
    token: String
  }

  
  input ResetPasswordInput {
    email: String
    password: String
    token: String
  }

  input VerifyTasterInput {
    token: String
    emailAddress: String
  }

  input CompleteProfileInput {
    id: ID
    fullName: String
    nationality: String
    birthYear: Int
    gender: String
  }

  input EditUserInput {
    id: ID
    emailAddress: String
    fullName: String
    birthYear: Int
    nationality: String
    gender: String
    initialized: Boolean
    type: String
    isSuperAdmin: Boolean
    isTaster: Boolean
    organization: ID
    country: String
    state: String
    language: String
    paypalEmailAddress: String
    isProfileCompleted: Boolean
  }

  type ProductRewardsRule {
    min: Float
    max: Float
    percentage: Float
    active: Boolean
  }

  type SurveyEnrolmentLogIn {
    id: ID!
    survey: Survey
    state: String
    lastAnsweredQuestion: Question
    user: User
    selectedProducts: [Product]
    lastSelectedProduct: String
    paypalEmail: String
    referral: ID
    savedRewards: JSON
    savedQuestions: JSON
    answers: JSON
    productDisplayOrder: [String]
    productDisplayType: String
    productRewardsRule: ProductRewardsRule
  }

  extend type Mutation {
    requestAccount(input: RequestAccountInput): Boolean
    loginToSurvey(
      email: String
      survey: String!
      referral: ID
      extsource: String
      extid: String
      isUserLoggedIn: Boolean
      password: String
      country: String
      browserInfo: JSON
    ): SurveyEnrolmentLogIn
    loginUser(email: String!, password: String!): AuthPayload
    forgotPassword(email: String!): Boolean
    resetPassword(input: ResetPasswordInput): Boolean
    verifyTaster(input: VerifyTasterInput): AuthPayload
    completeProfile(input: CompleteProfileInput): User
    addUser(input: EditUserInput): User
    deleteUser(id: ID!): Boolean
    unarchivedUser(id: ID!): Boolean
    editUser(input: EditUserInput): User
    blackListUser(id: ID!):Boolean

    createTasterAccount(input: CreateTasterAccountInput): AuthPayload
    tasterResendVerificationEmail(email: String!): AuthPayload
    updateTasterAccount(input: UpdateTasterAccountInput): User
    updateUserPassword(currentPassword: String!, newPassword: String!): Boolean
    isUserRegistered(email: String!): Boolean
  }
`

module.exports = User
