const Feedback = /* GraphQL */ `
  type Feedback {
    reason: String!
    email: String!
    rating: Float!
  }

  extend type Mutation {
    addFeedback(reason: String!, email: String!, rating: Float!, token: String!): Feedback
  }
`

module.exports = Feedback
