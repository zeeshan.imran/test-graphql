const Product = /* GraphQL */ `
  type Product {
    id: ID!
    name: String!
    photo: String # url for picture
    brand: ID
    reward: Float
    isAvailable: Boolean
    isSurveyCover: Boolean
    sortingOrderId: Int
    internalName: String
    rejectedReward: Float
    delayToNextProduct: String
    extraDelayToNextProduct: String
    amountInDollar: Float
  }

  type FunnelProduct {
    id: ID
    name: String
    responses: Int
  }

  type PopulatedProduct {
    id: ID!
    name: String!
    photo: String # url for picture
    brand: Brand
    reward: Float
    isAvailable: Boolean
    isSurveyCover: Boolean
    sortingOrderId: Int
    internalName: String
    rejectedReward: Float
    delayToNextProduct: String
    extraDelayToNextProduct: String
    amountInDollar: Float
  }

  input ProductInput {
    id: ID
    name: String
    brand: String
    photo: String
    reward: Float
    isAvailable: Boolean
    isSurveyCover: Boolean
    sortingOrderId: Int
    internalName: String
    rejectedReward: Float
    delayToNextProduct: String
    extraDelayToNextProduct: String
    amountInDollar: Float
  }

  input ReplaceProductInput {
    id: ID
    clientGeneratedId: ID
    name: String
    brand: String
    photo: String
    reward: Float
    isAvailable: Boolean
    isSurveyCover: Boolean
    sortingOrderId: Int
    internalName: String
    rejectedReward: Float
    delayToNextProduct: String
    extraDelayToNextProduct: String
    amountInDollar: Float
  }

  # TODO: refactor to remove createProduct, updateProduct, createProducts
  type ReplaceProductPayload {
    clientGeneratedId: ID
    product: Product!
  }

  extend type Query {
    product(id: ID): PopulatedProduct
    products: [PopulatedProduct]
  }

  extend type Mutation {
    createProduct(input: ProductInput): Product
    createProducts(input: [ProductInput]): [Product]
    updateProduct(input: ProductInput): Product
    replaceProducts(input: [ReplaceProductInput!]): [ReplaceProductPayload!]!
  }
`

module.exports = Product
