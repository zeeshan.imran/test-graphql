const SurveyAnalysis = /* GraphQL */ `
  type AnalysisFile {
    type: String
    name: String
  }

  type AnalysisResult {
    id: ID!
    name: String
    status: String!
    files: [AnalysisFile!]
    message: String
    description: String
  }

  extend type Query {
    findSurveyAnalysisResults(jobGroup: ID!): [AnalysisResult!]
    getAnalysisFile(id: ID!, file: String!): JSON
  }

  input AnalysisRequest {
    analysisType: String!
    referenceQuestion: ID
    penaltyLevel: Float
    alphaLevel: Float
  }

  input CreateJobsInput {
    jobGroup: ID!
    question: ID!
    productIds: [String!]
    questionFilters: [Object]
    dateFilter: [Date]
    analysisRequests: [AnalysisRequest!]!
  }

  extend type Mutation {
    createJobs(input: CreateJobsInput!): JSON
  }
`

module.exports = SurveyAnalysis
