const CountrySurveyGrading = /* GraphQL */ `
  type ScoreRange {
    start: String
    end: String
  }

  type Grades {
    grade: String
    scoreRange: ScoreRange
  }

  type CountrySurveyGrading {
    id: ID!
    country: String
    grades: [Grades]
    owner: Organization
    createdBy: User
    updatedBy: User
    createdAt: Float
    updatedAt: Float
  }

  extend type Query {
    getCountrySurveyGrading: [CountrySurveyGrading]
  }

  input ScoreRangeInput {
    start: String!
    end: String!
  }

  input GradesInput {
    grade: String!
    scoreRange: ScoreRangeInput
  }

  input CreateCountrySurveyGradingInput {
    country: String!
    grades: [GradesInput!]
  }

  input UpdateCountrySurveyGradingInput {
    id: ID!
    country: String
    grades: [GradesInput]
  }

  extend type Mutation {
    addCountrySurveyGrading(input: CreateCountrySurveyGradingInput!): CountrySurveyGrading
    updateCountrySurveyGrading(input: UpdateCountrySurveyGradingInput): CountrySurveyGrading
  }
`

module.exports = CountrySurveyGrading
