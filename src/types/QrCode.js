const QrCode = /* GraphQL */ `
  enum TargetType {
    link
    survey
  }

  type QrCode {
    id: ID!
    name: String!
    uniqueName: String!
    owner: Organization
    targetType: TargetType
    survey: Survey
    targetLink: String
    redirectLink: String!
    qrCodePhoto: String!
  }

  type TargetLink {
    id: ID!
    type: String!
    url: String!
  }

  input QrCodeCreateInput {
    name: String!
    uniqueName: String!
    targetType: TargetType!
    survey: String
    targetLink: String
  }

  input QrCodeUpdateInput {
    name: String
    uniqueName: String
    targetType: TargetType
    survey: String
    targetLink: String
  }

  enum QrCodeOrderField {
    name
  }

  input QrCodesInput {
    keyword: String
    #
    orderBy: QrCodeOrderField
    orderDirection: OrderDirection
    skip: Int
    limit: Int
  }

  input CountQrCodesInput {
    keyword: String
  }

  enum ImageFormat {
    eps
    png
    svg
  }

  extend type Query {
    targetLink(organization: String!, qrCode: String!): TargetLink!
    qrCode(id: ID!, checkOrg: Boolean): QrCode
    qrCodes(input: QrCodesInput!): [QrCode!]!
    countQrCodes(input: CountQrCodesInput!): Int!
  }

  extend type Mutation {
    downloadQrCode(id: ID!, format: ImageFormat): String!
    createQrCode(input: QrCodeCreateInput): QrCode
    updateQrCode(id: ID!, input: QrCodeUpdateInput): QrCode
    deleteQrCode(id: ID!): ID
  }
`

module.exports = QrCode
