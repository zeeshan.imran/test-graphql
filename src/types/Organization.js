const Organization = /* GraphQL */ `
  type Organization {
    id: ID!
    name: String
    uniqueName: String!
    members(types: [String]): [User]
    createdBy: User
    modifiedBy: User
    usersNumber: Int
  }

  enum OrganizationOrderField {
    name
    createdBy
    modifiedBy
    usersNumber
  }

  input OrganizationsInput {
    keyword: String
    #
    orderBy: OrganizationOrderField
    orderDirection: OrderDirection
    skip: Int
    limit: Int
    type: [String!]
    inactive: Boolean
  }

  input CountOrganizationsInput {
    keyword: String
    type: [String!]
    inactive: Boolean
  }

  extend type Query {
    organization(id: ID!): Organization
    organizations(input: OrganizationsInput!): [Organization]
    countOrganizations(input: CountOrganizationsInput!): Int
  }

  extend type Mutation {
    addOrganization(name: String!, uniqueName: String!): Boolean
    editOrganization(name: String!, id: ID!): Organization
    deleteOrganization(id: ID!): Boolean
    unarchivedOrganization(id: ID!): Boolean
  }
`
module.exports = Organization
