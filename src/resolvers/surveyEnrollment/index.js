const rp = require('request-promise')
const env = require('../../config/env')
const getApiHeaders = require('../../utils/getApiHeaders')
const tastingProfileData = require('./tastingProfileData')
const sliderProfileData = require('./sliderProfileData')
const submitAnswer = require('./submitAnswer')
const finishSurvey = require('./finishSurvey')
const rejectSurvey = require('./rejectSurvey')
const finishSurveyScreening = require('./finishSurveyScreening')

module.exports = {
  SurveyEnrollment: {
    survey: async enrollment => {
      const { survey } = enrollment
      // getting survey by ID
      return rp({
        method: 'GET',
        uri: `${process.env.API_URL}/survey/${survey}`,
        json: true
      })
    }
  },
  Query: {
    surveyEnrollments: async () => {
      const res = await rp({
        method: 'GET',
        uri: `${process.env.API_URL}/surveyEnrollment`,
        json: true
      })

      return res
    },
    surveyEnrollment: async (_, { id }) => {
      const res = await rp({
        method: 'GET',
        uri: `${process.env.API_URL}/surveyEnrollment/${id}`,
        json: true
      })

      return res
    },
    surveyEnrollmentValidations: async (_, { surveyId, params }, { headers }) => {
      try {
        const query = JSON.stringify(params)
        const res = await rp({
          method: 'GET',
          headers: getApiHeaders(headers),
          uri: `${process.env.API_URL}/survey/${surveyId}/survey-enrollment-validations?query=${query}`,
          json: true
        })
        return res
      } catch (error) {
        throw new Error(error.error)
      }
    },
    countSurveyEnrollmentValidations: async (_, { surveyId, params }, { headers }) => {
      try {
        const query = JSON.stringify(params)
        const res = await rp({
          method: 'GET',
          headers: getApiHeaders(headers),
          uri: `${process.env.API_URL}/survey/${surveyId}/count-survey-enrollment-validations?query=${query}`,
          json: true
        })
        return res
      } catch (error) {
        throw new Error(error.error)
      }
    },
    surveyFunnel: async (_, params, { headers }) => {
      try {
        const query = JSON.stringify(params)
        const res = await rp({
          method: 'GET',
          headers: getApiHeaders(headers),
          uri: `${process.env.API_URL}/surveyenrollement/funnel?query=${query}`,
          json: true
        })
        return res
      } catch (error) {
        throw new Error(error.error)
      }
    },
    userSurveyEnrollments: async (_, { userId }, { headers }) => {
      try {
        const response = await rp({
          method: 'GET',
          uri: `${process.env.API_URL}/surveyenrollement/user/${userId}`,
          headers: getApiHeaders(headers),
          json: true
        })

        return response
      } catch (error) {
        throw new Error(error.error)
      }
    },
    surveyFunnelValidation: async (_, params, { headers }) => {
      try {
        const query = JSON.stringify(params)
        const res = await rp({
          method: 'GET',
          headers: getApiHeaders(headers),
          uri: `${process.env.API_URL}/surveyenrollement/funnelValidation?query=${query}`,
          json: true
        })
        return res
      } catch (error) {
        throw new Error(error.error)
      }
    },
    tastingProfileData,
    sliderProfileData
  },
  Mutation: {
    saveQuestions: async (_, { questions, surveyEnrollment }) => {
      const res = await rp({
        method: 'POST',
        uri: `${process.env.API_URL}/surveyenrollment/saveQuestions`,
        body: { questions, surveyEnrollment },
        json: true
      })

      return res
    },
    saveRewards: async (_, { rewards, surveyEnrollment }) => {
      const res = await rp({
        method: 'POST',
        uri: `${process.env.API_URL}/surveyenrollment/saveRewards`,
        body: { rewards, surveyEnrollment },
        json: true
      })

      return res
    },
    setSelectedProducts: async (_, { surveyEnrollment, selectedProducts }) => {
      const res = await rp({
        method: 'POST',
        uri: `${process.env.API_URL}/surveyenrollment/selectedproducts`,
        body: {
          surveyEnrollment,
          selectedProducts
        },
        json: true
      })

      return res
    },
    setPaypalEmail: async (_, { surveyEnrollment, paypalEmail }) => {
      const res = await rp({
        method: 'POST',
        uri: `${process.env.API_URL}/surveyenrollment/paypalemail`,
        body: {
          surveyEnrollment,
          paypalEmail
        },
        json: true
      })
      return res
    },
    updateValidateSurveyEnrollement: async (_, { input }, { headers }) => {
      try {
        const res = await rp({
          method: 'POST',
          headers: getApiHeaders(headers),
          body: input,
          uri: `${process.env.API_URL}/surveyenrollement/validation/${input.id}`,
          json: true
        })

        const getOnlyAnsweredProduct =
          input &&
          input.savedRewards.length &&
          input.savedRewards.filter(reward => reward.answered)

        const answeredProductValidation =
          getOnlyAnsweredProduct &&
          getOnlyAnsweredProduct.length &&
          getOnlyAnsweredProduct.map(productAnswer => ({
            product_id: productAnswer.id,
            show_on_charts: productAnswer.showOnCharts
          }))

        const respondentId = input.id
        const surveyId = input.surveyId
        await rp({
          method: 'POST',
          uri: `${env.STATS_SERVER_API}/survey/${surveyId}/respondent/${respondentId}/validation`,
          body: {
            validation: input.validation === 'valid' ? 1 : 0,
            processed: input.processed,
            escalation: input.escalation,
            hiddenFromCharts: input.hiddenFromCharts || false,
            product_validation: answeredProductValidation
          },
          json: true
        })
        return res
      } catch (error) {
        throw new Error(error)
      }
    },
    updateSurveyEnrolmentProductIncentiveStatus: async (
      _,
      { surveyID, enrollments },
      { headers }
    ) => {
      try {
        const res = await rp({
          method: 'POST',
          headers: getApiHeaders(headers),
          body: { surveyID, enrollments },
          uri: `${process.env.API_URL}/surveyenrollement/product-incentive-payment/${surveyID}`,
          json: true
        })
        return res
      } catch (error) {
        throw new Error(error.error)
      }
    },
    updateSurveyEnrolmentGiftCardIncentiveStatus: async (
      _,
      { surveyID, enrollments },
      { headers }
    ) => {
      try {
        const res = await rp({
          method: 'POST',
          headers: getApiHeaders(headers),
          body: { surveyID, enrollments },
          uri: `${process.env.API_URL}/surveyenrollement/gift-card-incentive-payment/${surveyID}`,
          json: true
        })
        return res
      } catch (error) {
        throw new Error(error.error)
      }
    },
    updateSurveyShareStatus: async (
      _,
      { surveyID, enrollments },
      { headers }
    ) => {
      try {
        const res = await rp({
          method: 'POST',
          headers: getApiHeaders(headers),
          body: { surveyID, enrollments },
          uri: `${process.env.API_URL}/surveyenrollement/survey-shares-payment/${surveyID}`,
          json: true
        })
        return res
      } catch (error) {
        throw new Error(error.error)
      }
    },
    submitForcedSignUp: async (
      _,
      { input: { surveyEnrollment, email, password, country } }
    ) => {
      try {
        const url = `${process.env.API_URL}/surveyenrollement/signuptosurvey`

        await rp({
          method: 'POST',
          uri: url,
          body: {
            surveyEnrollment,
            email,
            password,
            country
          },
          json: true
        })
        return true
      } catch (error) {
        throw new Error(error)
      }
    },
    updateUserEnrollementExpiry: async (_, { input }, { headers }) => {
      try {
        const { enrollmentId, days } = input
        const response = await rp({
          method: 'PATCH',
          headers: getApiHeaders(headers),
          uri: `${process.env.API_URL}/surveyenrollement/${enrollmentId}/expiry`,
          body: {
            allowedDaysToFillTheTasting: Number(days)
          },
          json: true
        })
        return response
      } catch (error) {
        throw new Error(error)
      }
    },
    submitAnswer,
    finishSurvey,
    rejectSurvey,
    finishSurveyScreening
  }
}
