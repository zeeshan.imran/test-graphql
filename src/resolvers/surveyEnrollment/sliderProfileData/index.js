const rp = require('request-promise')
const { keys } = require('ramda')

const submitAnalyticsRequest = async body => {
  return rp({
    headers: {
      Authorization: 'u5QaEI3dQ20fRNnABDC2ANag62bX3lYtXgS2LcwPS0c'
    },
    method: 'POST',
    uri: `https://api.flavorwiki.com/v1/slider-question/`,
    body,
    json: true
  })
}

const sliderProfileData = async (_, { input }) => {
  const { enrollment, survey, question, product, labels, profileName } = input

  const requestBody = {
    action: 'profile',
    pair_data: {
      respondent_id: enrollment,
      survey_id: survey,
      question_id: question,
      product_id: product,
      attributes: labels.reduce(
        (asObject, label) => ({
          ...asObject,
          [label]: [label]
        }),
        {}
      )
    }
  }

  const response = await submitAnalyticsRequest(requestBody)
  const responseLabels = keys(response.profile)

  const formattedResponse = {
    labels: responseLabels,
    series: [
      {
        name: profileName,
        data: responseLabels.map(label => response.profile[label])
      }
    ]
  }

  return formattedResponse
}

module.exports = sliderProfileData
