const rp = require('request-promise')
const { concat, uniq } = require('ramda')
const env = require('../../../config/env')

const getUnanswerdPairs = (allPairs, answeredPairsIds) => {
  const allPairsById = allPairs.reduce((byId, pair) => {
    return Object.assign({}, byId, { [pair.id]: pair })
  }, {})

  const mixedPairs = concat(allPairs.map(({ id }) => id), answeredPairsIds)

  const notAnsweredPairsIds = uniq(mixedPairs)
  const notAnsweredPairs = notAnsweredPairsIds.map(id => allPairsById[id])

  return notAnsweredPairs
}

const submitPairsToAnalyze = async ({
  survey,
  surveyEnrollment,
  question,
  product,
  dataSet
}) => {
  const returnValue = await rp({
    method: 'POST',
    uri: `${
      env.STATS_SERVER_API
    }/survey/${survey}/respondent/${surveyEnrollment}/pairs/${question}/${product}`,
    body: dataSet,
    json: true
  })
  return returnValue
}

const submitPairsSkippedToAnalyze = async ({
  survey,
  surveyEnrollment,
  question,
  product,
  skippedPairs
}) => {
  const returnValue = await rp({
    method: 'POST',
    uri: `${
      env.STATS_SERVER_API
    }/survey/${survey}/respondent/${surveyEnrollment}/skipped-pairs/${question}/${product}`,
    body: skippedPairs,
    json: true
  })
  return returnValue
}

const submitSlidersToAnalyze = async ({
  surveyId,
  surveyEnrollment,
  selectedProduct,
  question,
  answersOfSliders
}) => {
  return rp({
    method: 'POST',
    uri: `${
      env.STATS_SERVER_API
    }/survey/${surveyId}/respondent/${surveyEnrollment}/sliders/${question}/${selectedProduct}`,
    body: answersOfSliders,
    json: true
  })
}

const submitProfileData = async ({
  surveyEnrollment,
  survey,
  product,
  question,
  pairs
}) => {
  const dataSet = []
  pairs.forEach(singlePair => {
    const { value, pair, timeToAnswer, optionId } = singlePair
    const winnerId = optionId
    const loserId =
      pair.leftAttribute.optionId !== optionId
        ? pair.leftAttribute.optionId
        : pair.rightAttribute.optionId
    const winner = value
    const loser =
      pair.leftAttribute.pair !== value
        ? pair.leftAttribute.pair
        : pair.rightAttribute.pair
    dataSet.push({
      winner,
      loser,
      winner_id: winnerId,
      loser_id: loserId,
      items_order_ids: `${pair.leftAttribute.optionId},${
        pair.rightAttribute.optionId
      }`,
      items_order: `${pair.leftAttribute.pair},${pair.rightAttribute.pair}`,
      response_time: timeToAnswer
    })
  })
  if (dataSet.length) {
    submitPairsToAnalyze({
      survey,
      surveyEnrollment,
      product,
      question,
      dataSet
    })
  }
}

const sendPairQuestionsAnalytics = async ({
  pairs,
  value,
  surveyEnrollment,
  surveyId,
  questionId,
  startedAt,
  productId
}) => {
  let accTimeToAnswer = []
  const pairedAnswersSet = []
  const remainingPairs = getUnanswerdPairs(
    pairs,
    value.map(({ pairQuestion }) => pairQuestion)
  )

  for (let pairIndex = 0; pairIndex < value.length; pairIndex++) {
    const pairAnswer = value[pairIndex]
    const {
      pairQuestion,
      value: pairValue,
      timeToAnswer,
      optionId
    } = pairAnswer
    const pair = pairs.find(pair => pair.id === pairQuestion)
    accTimeToAnswer[pairIndex] =
      timeToAnswer + (accTimeToAnswer[pairIndex - 1] || 0)
    const startedPairAt = startedAt + accTimeToAnswer[pairIndex]

    pairedAnswersSet.push({
      pair: pair,
      value: pairValue,
      optionId,
      startedAt: startedPairAt,
      timeToAnswer
    })
  }

  if (pairedAnswersSet.length) {
    await submitProfileData({
      surveyEnrollment,
      survey: surveyId,
      question: questionId,
      product: productId,
      pairs: pairedAnswersSet
    })
  }

  await submitPairsSkippedToAnalyze({
    surveyEnrollment,
    survey: surveyId,
    question: questionId,
    product: productId,
    skippedPairs: remainingPairs.map(({ leftAttribute, rightAttribute }) => ({
      left: leftAttribute.pair,
      right: rightAttribute.pair
    }))
  })
}

const submitAnswer = async (
  _,
  {
    input: {
      question,
      context,
      surveyEnrollment,
      startedAt,
      value,
      selectedProduct,
      rejectedEnrollment
    }
  }
) => {
  try {
    if (context && context.save && context.type === 'slider' && context.save) {
      const {
        survey: { id: surveyId }
      } = await rp({
        method: 'GET',
        uri: `${process.env.API_URL}/surveyEnrollment/${surveyEnrollment}`,
        json: true
      })

      const answersOfSliders = value.map((val, index) => ({
        slider: index,
        value: val.value
      }))

      await submitSlidersToAnalyze({
        surveyId,
        surveyEnrollment,
        selectedProduct,
        question,
        answersOfSliders
      })

      return true
    }
    await rp({
      method: 'POST',
      uri: `${process.env.API_URL}/surveyenrollment/submitanswer`,
      body: {
        question,
        surveyEnrollment,
        value,
        startedAt,
        selectedProduct,
        rejectedEnrollment
      },
      json: true
    })

    if (context && context.type === 'paired-questions') {
      // Gets the surveyId from the surveyEnrollment
      const {
        survey: { id: surveyId }
      } = await rp({
        method: 'GET',
        uri: `${process.env.API_URL}/surveyEnrollment/${surveyEnrollment}`,
        json: true
      })

      sendPairQuestionsAnalytics({
        pairs: context.pairs,
        value,
        surveyEnrollment,
        surveyId,
        questionId: question,
        startedAt,
        productId: selectedProduct
      })
    }

    return true
  } catch (error) {
    throw new Error(error.error && error.error.code)
  }
}

module.exports = submitAnswer
