const rp = require('request-promise')
const { keys } = require('ramda')
const env = require('../../../config/env')

const submitAnalyticsRequest = async ({
  survey,
  enrollment,
  question,
  product
}) => {
  return rp({
    method: 'GET',
    uri: `${
      env.STATS_SERVER_API
    }/survey/${survey}/respondent/${enrollment}/pairs/${question}/${product}/profiles`,
    json: true
  })
}

const tastingProfileData = async (_, { input }) => {
  const { enrollment, survey, question, product, labels, profileName } = input

  const response = await submitAnalyticsRequest({
    survey,
    enrollment,
    question,
    product,
    labels
  })
  const responseLabels = keys(response.profile)

  const formattedResponse = {
    labels: responseLabels,
    series: [
      {
        name: profileName,
        data: responseLabels.map(label => response.profile[label])
      }
    ]
  }

  return formattedResponse
}

module.exports = tastingProfileData
