const rp = require('request-promise')

const finishSurvey = async (_, { input }) => {
  try {
    const { surveyEnrollment } = input

    const { survey: { id: surveyId } = {}, user } = await rp({
      method: 'GET',
      uri: `${process.env.API_URL}/surveyEnrollment/${surveyEnrollment}`,
      json: true
    })

    if (user && user.emailAddress) {
      await rp({
        method: 'POST',
        uri: `${process.env.API_URL}/email`,
        body: {
          email: user.emailAddress,
          id: surveyId,
          type: 'survey-completed',
          surveyEnrollment
        },
        json: true
      })
    }

    const response = await rp({
      method: 'POST',
      uri: `${process.env.API_URL}/surveyenrollment/finishsurvey`,
      body: {
        surveyEnrollment
      },
      json: true
    })

    return response
  } catch (error) {
    throw new Error(error.error)
  }
}

module.exports = finishSurvey
