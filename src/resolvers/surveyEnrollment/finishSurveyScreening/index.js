const rp = require('request-promise')

const finishSurveyScreening = async (_, { input }) => {
  try {
    const { surveyEnrollment } = input

    const { survey: { id: surveyId } = {}, state, user } = await rp({
      method: 'GET',
      uri: `${process.env.API_URL}/surveyEnrollment/${surveyEnrollment}`,
      json: true
    })

    if (state !== 'waiting-for-product' && user && user.emailAddress) {
      rp({
        method: 'POST',
        uri: `${process.env.API_URL}/email`,
        body: {
          email: user.emailAddress,
          id: surveyId,
          type: 'survey-waiting',
          surveyEnrollment
        },
        json: true
      }).catch(e => {
        // TODO: send to applog (Sentry for example)
      })
    }

    rp({
      method: 'POST',
      uri: `${process.env.API_URL}/surveyenrollment/finishsurveyscreening`,
      body: {
        surveyEnrollment
      },
      json: true
    })
  } catch (error) {
    throw new Error(error.error)
  }
}

module.exports = finishSurveyScreening
