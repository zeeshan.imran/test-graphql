const rp = require('request-promise')
const getApiHeaders = require('../../utils/getApiHeaders')

module.exports = {
  Query: {
    getCountrySurveyGrading: async (_, __, { headers }) => {
      try {
        const response = await rp({
          method: 'GET',
          headers: getApiHeaders(headers),
          url: `${process.env.API_URL}/country/survey/grading`,
          json: true
        })
        return response
      } catch (ex) {
        throw new Error(ex.error)
      }
    }
  },
  Mutation: {
    addCountrySurveyGrading: async (_, { input }, { headers }) => {
      try {
        const addGrading = await rp({
          headers: getApiHeaders(headers),
          method: 'POST',
          uri: `${process.env.API_URL}/country/survey/grading`,
          body: { ...input },
          json: true
        })
        return addGrading
      } catch (ex) {
        throw new Error(ex.error)
      }
    },
    updateCountrySurveyGrading: async (_, { input }, { headers }) => {
      try {
        const { id, grades, country } = input
        const updateGrading = await rp({
          headers: getApiHeaders(headers),
          method: 'PATCH',
          uri: `${process.env.API_URL}/country/survey/grading/${id}`,
          body: { grades, country },
          json: true
        })
        return updateGrading
      } catch (e) {
        throw new Error(e.error)
      }
    }
  }
}
