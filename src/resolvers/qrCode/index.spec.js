/* eslint-env jest */
const { graphql } = require('graphql')
const nock = require('nock')
const schema = require('../../schema')
const createLoaders = require('../../loaders')

const qrCodes = [
  {
    id: 'qr-code-1',
    name: 'Qr Code 1',
    uniqueName: 'qr-code-1',
    owner: 'org-1',
    targetType: 'survey',
    survey: 'survey-1',
    qrCodePhoto: 'https://fake-aws-s3/bucket/qr-codes/org-1/qr-code-1.png'
  },
  {
    id: 'qr-code-2',
    name: 'Qr Code 2',
    uniqueName: 'qr-code-2',
    owner: 'org-1',
    targetType: 'link',
    targetLink: 'https://flavor-wiki.slack.com',
    qrCodePhoto: 'https://fake-aws-s3/bucket/qr-codes/org-1/qr-code-2.png'
  }
]

describe('Qr Codes', () => {
  let context

  beforeEach(() => {
    const headers = { Authorization: 'Bearer token' }
    context = { headers, ...createLoaders({ headers }) }

    nock(process.env.API_URL)
      .get('/qrCode')
      .reply(200, qrCodes)

      .get('/qrCode')
      .query({ query: '{"keyword":""}' })
      .reply(200, qrCodes)

      .get('/qrCode/qr-code-1')
      .query(true)
      .reply(200, qrCodes[0])

      .get('/qrCode/getTargetLink')
      .query({ organization: 'org-1', qrCode: 'qr-code-1' })
      .reply(200, {
        id: 'qr-code-1',
        type: 'survey',
        url: '/survey/the-target-survey'
      })

      .get('/organization')
      .query(true)
      .reply(200, [
        { id: 'org-1', name: 'Organization 1', uniqueName: 'org-1' }
      ])

      .get('/survey/survey-1')
      .reply(200, {
        id: 'survey-1',
        uniqueName: 'survey-1',
        name: 'Survey 1'
      })
  })

  afterEach(() => {
    nock.cleanAll()
  })

  test('search Qr Codes', async () => {
    const QUERY = /* GraphQL */ `
      query {
        qrCodes(input: { keyword: "" }) {
          id
          name
          uniqueName
          owner {
            id
            name
            uniqueName
          }
          targetType
          survey {
            id
            uniqueName
            name
          }
          targetLink
          redirectLink
          qrCodePhoto
        }
      }
    `

    const result = await graphql(schema, QUERY, {}, context)

    expect(result.data.qrCodes).toHaveLength(2)
    expect(result.data.qrCodes[0]).toMatchObject({
      owner: {
        id: 'org-1',
        uniqueName: 'org-1',
        name: 'Organization 1'
      },
      survey: { id: 'survey-1', name: 'Survey 1' },
      redirectLink: '/qr-code/org-1/qr-code-1'
    })
    expect(result.data.qrCodes[1]).toMatchObject({
      owner: {
        id: 'org-1',
        uniqueName: 'org-1',
        name: 'Organization 1'
      },
      survey: null,
      redirectLink: '/qr-code/org-1/qr-code-2'
    })
  })

  test('get target link of Qr Code', async () => {
    const QUERY = /* GraphQL */ `
      query {
        targetLink(organization: "org-1", qrCode: "qr-code-1") {
          id
          type
          url
        }
      }
    `

    const result = await graphql(schema, QUERY, {}, context)
    expect(result.data.targetLink).toMatchObject({
      id: 'qr-code-1',
      type: 'survey',
      url: '/survey/the-target-survey'
    })
  })

  test('get Qr Code', async () => {
    const QUERY = /* GraphQL */ `
      query {
        qrCode(id: "qr-code-1") {
          id
          name
          uniqueName
          owner {
            id
            name
            uniqueName
          }
          targetType
          survey {
            id
            uniqueName
            name
          }
          targetLink
          redirectLink
          qrCodePhoto
        }
      }
    `

    const result = await graphql(schema, QUERY, {}, context)
    expect(result.data.qrCode).toMatchObject({
      owner: {
        id: 'org-1',
        uniqueName: 'org-1',
        name: 'Organization 1'
      },
      survey: { id: 'survey-1', name: 'Survey 1' },
      redirectLink: '/qr-code/org-1/qr-code-1'
    })
  })

  test('create Qr Code', async () => {
    nock(process.env.API_URL)
      .post('/qrCode')
      .reply(200, (_, requestBody) => ({
        id: 'qr-code-3',
        owner: 'org-1',
        ...requestBody
      }))

    const QUERY = /* GraphQL */ `
      mutation {
        createQrCode(
          input: {
            name: "Qr Code 3"
            uniqueName: "qr-code-3"
            targetType: link
            targetLink: "https://google.com/"
          }
        ) {
          id
          name
          uniqueName
          owner {
            id
            name
            uniqueName
          }
          redirectLink
        }
      }
    `

    const result = await graphql(schema, QUERY, {}, context)

    expect(result.data.createQrCode).toMatchObject({
      id: 'qr-code-3',
      name: 'Qr Code 3',
      uniqueName: 'qr-code-3',
      owner: {
        id: 'org-1',
        name: 'Organization 1',
        uniqueName: 'org-1'
      },
      redirectLink: '/qr-code/org-1/qr-code-3'
    })
  })

  test('should throw error when the creating Qr code api returns error', async () => {
    nock(process.env.API_URL)
      .post('/qrCode')
      .reply(400, { code: 'RETURN_ERROR_CODE' })

    const QUERY = /* GraphQL */ `
      mutation {
        createQrCode(
          input: {
            name: "Qr Code 3"
            uniqueName: "qr-code-3"
            targetType: link
            targetLink: "https://google.com/"
          }
        ) {
          id
        }
      }
    `

    const result = await graphql(schema, QUERY, {}, context)

    expect(result.errors).toHaveLength(1)
    expect(result.errors[0]).toMatchObject({ message: 'RETURN_ERROR_CODE' })
  })

  test('update Qr Code', async () => {
    nock(process.env.API_URL)
      .patch('/qrCode/qr-code-1')
      .reply(200, (_, requestBody) => ({
        ...qrCodes[0],
        ...requestBody
      }))

    const QUERY = /* GraphQL */ `
      mutation {
        updateQrCode(
          id: "qr-code-1"
          input: {
            name: "Qr Code"
            uniqueName: "qr-code"
            targetType: link
            targetLink: "https://google.com/"
          }
        ) {
          id
          name
          uniqueName
          owner {
            id
            name
            uniqueName
          }
          redirectLink
        }
      }
    `

    const result = await graphql(schema, QUERY, {}, context)
    expect(result.data.updateQrCode).toMatchObject({
      id: 'qr-code-1',
      name: 'Qr Code',
      uniqueName: 'qr-code',
      owner: {
        id: 'org-1',
        name: 'Organization 1',
        uniqueName: 'org-1'
      },
      redirectLink: '/qr-code/org-1/qr-code'
    })
  })

  test('should throw error when the updating Qr code api returns error', async () => {
    nock(process.env.API_URL)
      .patch('/qrCode/qr-code-1')
      .reply(400, { code: 'RETURN_ERROR_CODE' })

    const QUERY = /* GraphQL */ `
      mutation {
        updateQrCode(
          id: "qr-code-1"
          input: {
            name: "Qr Code"
            uniqueName: "qr-code"
            targetType: link
            targetLink: "https://google.com/"
          }
        ) {
          id
        }
      }
    `

    const result = await graphql(schema, QUERY, {}, context)

    expect(result.errors).toHaveLength(1)
    expect(result.errors[0]).toMatchObject({ message: 'RETURN_ERROR_CODE' })
  })

  test('delete Qr Code', async () => {
    nock(process.env.API_URL)
      .delete('/qrCode/qr-code-1')
      .reply(200)

    const QUERY = /* GraphQL */ `
      mutation {
        deleteQrCode(id: "qr-code-1")
      }
    `

    const result = await graphql(schema, QUERY, {}, context)
    expect(result.data.deleteQrCode).toBe('qr-code-1')
  })

  test('should throw error when the deleting Qr code api returns error', async () => {
    nock(process.env.API_URL)
      .delete('/qrCode/qr-code-1')
      .reply(400, { code: 'RETURN_ERROR_CODE' })

    const QUERY = /* GraphQL */ `
      mutation {
        deleteQrCode(id: "qr-code-1")
      }
    `

    const result = await graphql(schema, QUERY, {}, context)

    expect(result.errors).toHaveLength(1)
    expect(result.errors[0]).toMatchObject({ message: 'RETURN_ERROR_CODE' })
  })
})
