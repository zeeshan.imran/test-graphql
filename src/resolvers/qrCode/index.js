const rp = require('request-promise')
const getApiHeaders = require('../../utils/getApiHeaders')

const base64Prefix = {
  eps: 'data:image/postscript;base64,',
  png: 'data:image/png;base64,',
  svg: 'data:image/svg+xml;base64,'
}

module.exports = {
  QrCode: {
    owner: async (qrCode, _args, { organizationLoader }) =>
      organizationLoader.load(qrCode.owner),
    survey: async (qrCode, _args) => {
      if (!qrCode.survey) {
        return null
      }

      try {
        return await rp({
          method: 'GET',
          uri: `${process.env.API_URL}/survey/${qrCode.survey}`,
          json: true
        })
      } catch (e) {
        return null
      }
    },
    redirectLink: async (qrCode, _args, { organizationLoader }) => {
      const organization = await organizationLoader.load(qrCode.owner)
      return `/qr-code/${organization.uniqueName}/${qrCode.uniqueName}`
    }
  },
  Query: {
    qrCodes: async (_, { input }, { headers }) => {
      try {
        const query = JSON.stringify(input)
        const res = await rp({
          method: 'GET',
          headers: getApiHeaders(headers),
          uri: `${process.env.API_URL}/qrCode?query=${query}`,
          json: true
        })
        return res
      } catch (error) {
        throw new Error(error.error && error.error.code)
      }
    },
    countQrCodes: async (_, { input }, { headers }) => {
      try {
        const query = JSON.stringify(input)
        const res = await rp({
          method: 'GET',
          headers: getApiHeaders(headers),
          uri: `${process.env.API_URL}/qrCode/count?query=${query}`,
          json: true
        })
        return res
      } catch (error) {
        throw new Error(error.error && error.error.code)
      }
    },
    qrCode: async (_, { id, checkOrg }, { headers }) => {
      try {
        let url = `${process.env.API_URL}/qrCode/${id}?populate=false`

        if (checkOrg) {
          url = `${
            process.env.API_URL
          }/qrCodeByOrganization/${id}?populate=false`
        }

        const res = await rp({
          method: 'GET',
          headers: getApiHeaders(headers),
          uri: url,
          json: true
        })
        return res
      } catch (error) {
        throw new Error(error)
      }
    },
    targetLink: async (_, { organization, qrCode }) => {
      try {
        const res = await rp({
          method: 'GET',
          uri: `${
            process.env.API_URL
          }/qrCode/getTargetLink?organization=${organization}&qrCode=${qrCode}`,
          json: true
        })
        return res
      } catch (error) {
        throw new Error(error.error && error.error.code)
      }
    }
  },
  Mutation: {
    downloadQrCode: async (_, { id, format = 'png' }, { headers }) => {
      try {
        const res = await rp({
          method: 'GET',
          headers: getApiHeaders(headers),
          uri: `${process.env.API_URL}/qrCode/${id}/download?format=${format}`,
          encoding: null
        })
        return `${base64Prefix[format]}${res.toString('base64')}`
      } catch (error) {
        throw new Error(error.error && error.error.code)
      }
    },
    createQrCode: async (_, { input }, { headers }) => {
      try {
        return await rp({
          method: 'POST',
          headers: getApiHeaders(headers),
          uri: `${process.env.API_URL}/qrCode`,
          body: input,
          json: true
        })
      } catch (error) {
        throw new Error(error.error && error.error.code)
      }
    },
    updateQrCode: async (_, { id, input }, { headers }) => {
      try {
        return await rp({
          method: 'PATCH',
          headers: getApiHeaders(headers),
          uri: `${process.env.API_URL}/qrCode/${id}`,
          body: input,
          json: true
        })
      } catch (error) {
        throw new Error(error.error && error.error.code)
      }
    },
    deleteQrCode: async (_, { id }, { headers }) => {
      try {
        await rp({
          method: 'DELETE',
          headers: getApiHeaders(headers),
          uri: `${process.env.API_URL}/qrCode/${id}`,
          json: true
        })

        return id
      } catch (error) {
        throw new Error(error.error && error.error.code)
      }
    }
  }
}
