const rp = require('request-promise')

module.exports = {
  Query: {
    getSignedUrl: async (_, { type, surveyId }) => {
      const res = await rp({
        method: 'POST',
        body: {
          type,
          surveyId
        },
        uri: `${process.env.API_URL}/getsignedurl`,
        json: true
      })

      return res
    }
  },
  Mutation: {}
}
