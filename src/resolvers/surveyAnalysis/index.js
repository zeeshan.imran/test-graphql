const rp = require('request-promise')
const compact = require('lodash.compact')
const env = require('../../config/env')
const getApiHeaders = require('../../utils/getApiHeaders')

module.exports = {
  Query: {
    findSurveyAnalysisResults: async (_root, { jobGroup }) => {
      try {
        const rs = await rp({
          method: 'GET',
          uri: `${env.STATS_SERVER_API}/groupJobs/${jobGroup}`,
          json: true
        })
        return rs.map(({ data, ...other }) => ({
          ...other,
          ...data
        }))
      } catch (ex) {
        throw ex
      }
    },
    getAnalysisFile: async (_root, { id, file }) => {
      try {
        return await rp({
          method: 'GET',
          uri: `${env.STATS_SERVER_API}/job/${id}/${file}`,
          json: true
        })
      } catch (ex) {
        throw ex
      }
    }
  },
  Mutation: {
    createJobs: async (
      _root,
      {
        input: {
          analysisRequests,
          jobGroup: jobGroupId,
          question: questionId,
          productIds,
          questionFilters,
          dateFilter
        }
      },
      { headers }
    ) => {
      try {
        questionFilters =
          questionFilters.length &&
          questionFilters.map(filter => {
            if (filter.options && filter.options.length) {
              return filter
            }
          })

        const { survey } = await rp({
          headers: getApiHeaders(headers),
          method: 'GET',
          uri: `${process.env.API_URL}/question/${questionId}?populate=survey&select=survey`,
          json: true
        })

        const rs = await rp({
          method: 'POST',
          uri: `${env.STATS_SERVER_API}/groupJobs/${jobGroupId}/${questionId}`,
          body: {
            validation: survey.validatedData,
            analysisRequests,
            products: productIds,
            filterQuestions: compact(questionFilters),
            dateFilter
          },
          json: true
        })

        return rs
      } catch (ex) {
        throw ex
      }
    }
  }
}
