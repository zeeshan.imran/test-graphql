const rp = require('request-promise')
const getApiHeaders = require('../../utils/getApiHeaders')

module.exports = {
  Mutation: {
    addFeedback: async (_, { reason, rating, email, token }, { headers }) => {
      try {
        const response = await rp({
          method: 'POST',
          headers: getApiHeaders(headers),
          url: `${process.env.API_URL}/feedback`,
          body: { reason, rating, email, token },
          json: true
        })
        return response
      } catch (error) {
        throw new Error(error.error)
      }
    }
  }
}
