const rp = require('request-promise')

module.exports = {
  Query: {
    brands: async (_, { ids }) => {
      let brands = []
      if (ids) {
        for (const id of ids) {
          const brand = await rp({
            method: 'GET',
            uri: `${process.env.API_URL}/brand/${id}`,
            json: true
          })
          brands = [...brands, brand]
        }
      } else {
        brands = await rp({
          method: 'GET',
          uri: `${process.env.API_URL}/brand`,
          json: true
        })
      }
      return brands
    },
    brand: async (_, { id }) => {
      const res = await rp({
        method: 'GET',
        uri: `${process.env.API_URL}/brand/${id}`,
        json: true
      })

      return res
    }
  },
  Mutation: {}
}
