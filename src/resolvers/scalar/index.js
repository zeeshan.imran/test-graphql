const GraphQLJSON = require('graphql-type-json')
const { GraphQLScalarType } = require('graphql')

module.exports = {
  JSON: GraphQLJSON,
  Date: new GraphQLScalarType({
    name: 'Date',
    description: 'Date custom scalar type',
    parseValue (value) {
      return new Date(value)
    },
    serialize (value) {
      return new Date(value)
    }
  })
}
