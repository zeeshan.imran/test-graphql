const rp = require('request-promise')
const getApiHeaders = require('../../utils/getApiHeaders')

module.exports = {
  Query: {
    surveyLogs: async (_, { surveyId, input }, { headers }) => {
      try {
        const query = JSON.stringify(input)
        const res = await rp({
          method: 'GET',
          headers: getApiHeaders(headers),
          reconnect: true,
          uri: `${process.env.API_URL}/survey/${surveyId}/logs?query=${query}`,
          json: true
        })
        return res
      } catch (error) {
        throw new Error(error.error)
      }
    },
    countSurveyLogs: async (_, { surveyId, input }, { headers }) => {
      try {
        const query = JSON.stringify(input)
        const res = await rp({
          method: 'GET',
          headers: getApiHeaders(headers),
          reconnect: true,
          uri: `${process.env.API_URL}/survey/${surveyId}/countLogs?query=${query}`,
          json: true
        })

        return res
      } catch (error) {
        throw new Error(error.error)
      }
    },
    photoValidationLogs: async (_, { surveyEnrollmentId }, { headers }) => {
      try {
        const res = await rp({
          method: 'GET',
          headers: getApiHeaders(headers),
          reconnect: true,
          uri: `${process.env.API_URL}/surveyenrollment/${surveyEnrollmentId}/logs`,
          json: true
        })
        return res
      } catch (error) {
        throw new Error(error.error)
      }
    }
  },
  Mutation: {

  }
}
