/* eslint-env jest */
const { mergeDeepRight } = require('ramda')
const { graphql } = require('graphql')
const nock = require('nock')
const schema = require('../../schema')
const createLoaders = require('../../loaders')

const createFakeContext = (contextProps) => {
  const headers = { Authorization: 'Bearer faked-token' }
  return mergeDeepRight(contextProps, { headers, ...createLoaders({ headers }) })
}

describe('me', () => {
  it('return user information when query me', async () => {
    nock(process.env.API_URL)
      .get('/user/me')
      .reply(200, {
        id: 'admin-1',
        emailAddress: 'admin@flavorwiki.com',
        type: 'operator',
        organization: 'organization-1'
      })
      //
      .get('/organization')
      .query({
        where: '{"id":["organization-1"]}'
      })
      .reply(200, [
        {
          id: 'organization-1',
          name: 'Oraganization 1'
        }
      ])

    const query = /* GraphQL */ `
      query {
        me {
          id
          emailAddress
          type
          organization {
            id
          }
        }
      }
    `

    const result = await graphql(schema, query, {}, createFakeContext())

    expect(result.data).toMatchObject({
      me: {
        id: 'admin-1',
        emailAddress: 'admin@flavorwiki.com',
        type: 'operator',
        organization: {
          id: 'organization-1'
        }
      }
    })
  })

  it('should response errors if the impersonate does not exist', async () => {
    nock(process.env.API_URL)
      .get('/user/me')
      .reply(400, { code: 'UNAUTHORIZED' })

    const query = /* GraphQL */ `
      query {
        me {
          id
          emailAddress
          type
          organization {
            id
          }
        }
      }
    `

    const result = await graphql(schema, query, {}, { headers: {} })

    expect(result.errors).toHaveLength(1)
    expect(result.errors[0]).toMatchObject({ message: 'UNAUTHORIZED' })
  })
})

describe('impersonate', () => {
  it("should return impersonate user's information when query me {...}", async () => {
    nock(process.env.API_URL)
      .get('/user/me')
      .matchHeader('impersonate', 'operator-1')
      .reply(200, {
        id: 'operator-1',
        emailAddress: 'operator@flavorwiki.com',
        type: 'operator',
        organization: 'organization-1'
      })
      //
      .get('/organization')
      .query({
        where: '{"id":["organization-1"]}'
      })
      .reply(200, [
        {
          id: 'organization-1',
          name: 'Oraganization 1'
        }
      ])

    const query = /* GraphQL */ `
      query {
        me {
          id
          emailAddress
          type
          organization {
            id
          }
        }
      }
    `

    const result = await graphql(
      schema,
      query,
      {},
      createFakeContext({ headers: { Impersonate: 'operator-1' } })
    )

    expect(result.data).toMatchObject({
      me: {
        id: 'operator-1',
        emailAddress: 'operator@flavorwiki.com',
        type: 'operator',
        organization: {
          id: 'organization-1'
        }
      }
    })
  })
  it('should return admin information when query me (impersonate: false) {...}', async () => {
    nock(process.env.API_URL)
      .get('/user/me')
      // when impersonate === false -> should not pass impersonate to api
      .matchHeader('impersonate', value => value === undefined)
      .reply(200, {
        id: 'admin-1',
        emailAddress: 'admin-1@flavorwiki.com',
        type: 'operator',
        organization: 'organization-1'
      })
      //
      .get('/organization')
      .query({
        where: '{"id":["organization-1"]}'
      })
      .reply(200, [
        {
          id: 'organization-1',
          name: 'Oraganization 1'
        }
      ])

    const query = /* GraphQL */ `
      query {
        me(impersonate: false) {
          id
          emailAddress
          type
          organization {
            id
          }
        }
      }
    `

    const result = await graphql(
      schema,
      query,
      {},
      createFakeContext({ headers: { impersonate: 'operator-1' } })
    )

    expect(result.data).toMatchObject({
      me: {
        id: 'admin-1',
        emailAddress: 'admin-1@flavorwiki.com',
        type: 'operator',
        organization: {
          id: 'organization-1'
        }
      }
    })
  })
})

describe('loginToSurvey resolver', () => {
  it('get the user', async () => {
    nock(process.env.API_URL)
      .post('/user/logintosurvey')
      .reply(200, {
        user: {
          id: '321',
          emailAddress: 'test@flavorwiki.com'
        },
        state: 'waiting-for-screening',
        lastAnsweredQuestion: null
      })

    const mutation = /* GraphQL */ `
      mutation {
        loginToSurvey(email: "test@test.test", survey: "123") {
          user {
            id
            emailAddress
          }
          state
          lastAnsweredQuestion {
            id
          }
        }
      }
    `

    const result = await graphql(schema, mutation, {}, {})

    expect(result).toEqual({
      data: {
        loginToSurvey: {
          user: {
            id: '321',
            emailAddress: 'test@flavorwiki.com'
          },
          state: 'waiting-for-screening',
          lastAnsweredQuestion: null
        }
      }
    })
  })
})
