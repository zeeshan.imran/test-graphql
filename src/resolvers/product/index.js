const rp = require('request-promise')
const getApiHeaders = require('../../utils/getApiHeaders')

module.exports = {
  Query: {
    product: async (_, { id }) => {
      const res = await rp({
        method: 'GET',
        uri: `${process.env.API_URL}/product/${id}`,
        json: true
      })

      return res
    },
    products: async () => {
      const res = await rp({
        method: 'GET',
        uri: `${process.env.API_URL}/product/`,
        json: true
      })

      return res
    }
  },
  Mutation: {
    createProduct: async (_, { input: product }, { headers }) => {
      try {
        const createdProduct = await rp({
          headers: getApiHeaders(headers),
          method: 'POST',
          uri: `${process.env.API_URL}/product`,
          body: product,
          json: true
        })
        return createdProduct
      } catch (error) {
        throw new Error(error.error)
      }
    },
    createProducts: async (_, { input }, { headers }) => {
      try {
        let res = []

        for (const product of input) {
          const createdProduct = await rp({
            headers: getApiHeaders(headers),
            method: 'POST',
            uri: `${process.env.API_URL}/product`,
            body: product,
            json: true
          })
          res.push(createdProduct)
        }
        return res
      } catch (error) {
        throw new Error(error.error)
      }
    },
    updateProduct: async (_, { input: product }, { headers }) => {
      try {
        const updatedProduct = await rp({
          headers: getApiHeaders(headers),
          method: 'PATCH',
          uri: `${process.env.API_URL}/product/${product.id}`,
          body: product,
          json: true
        })

        return updatedProduct
      } catch (error) {
        throw new Error(error.error)
      }
    },
    replaceProducts: async (_, { input: products }, { headers }) => {
      try {
        const replacedProducts = await rp({
          headers: getApiHeaders(headers),
          method: 'POST',
          uri: `${process.env.API_URL}/product/replaceProducts`,
          body: { products },
          json: true
        })

        return replacedProducts
      } catch (error) {
        throw new Error(error.error)
      }
    }
  }
}
