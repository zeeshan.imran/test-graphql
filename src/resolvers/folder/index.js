const rp = require('request-promise')
const getApiHeaders = require('../../utils/getApiHeaders')

module.exports = {
  Query: {
    getFolderPath: async (_, { folderId }, { headers }) => {
      try {
        return await rp({
          method: 'GET',
          headers: getApiHeaders(headers),
          uri: `${process.env.API_URL}/folders/${folderId}/path`,
          json: true
        })
      } catch (error) {
        throw new Error(error)
      }
    },
    listFolders: async (_, { parentId }, { headers }) => {
      try {
        const apiUrl = parentId
          ? `${process.env.API_URL}/folders?parentId=${parentId}`
          : `${process.env.API_URL}/folders`
        return rp({
          method: 'GET',
          headers: getApiHeaders(headers),
          url: apiUrl,
          json: true
        })
      } catch (ex) {
        throw new Error(ex.error)
      }
    },
    showFolder: async (_, { parentId, state, keyword }, { headers }) => {
      try {
        let apiUrl = `${process.env.API_URL}/folder/survey/listings?`
        if (parentId) {
          apiUrl = `${apiUrl}parentId=${parentId}${
            state || keyword ? '&&' : ''
          }`
        }
        if (state) {
          apiUrl = `${apiUrl}state=${state}${keyword ? '&&' : ''}`
        }
        if (keyword) {
          apiUrl = `${apiUrl}keyword=${keyword}`
        }

        return await rp({
          method: 'GET',
          headers: getApiHeaders(headers),
          url: apiUrl,
          json: true
        })
      } catch (ex) {
        throw new Error(ex.error)
      }
    }
  },
  Mutation: {
    updateFolder: async (_, { name, parent, id }, { headers }) => {
      try {
        return rp({
          method: 'PUT',
          headers: getApiHeaders(headers),
          url: `${process.env.API_URL}/folders/${id}`,
          json: true,
          body: {
            name: name,
            parent: parent
          }
        })
      } catch (ex) {
        throw new Error(ex.error)
      }
    },
    createFolder: async (_, { name, parent }, { headers }) => {
      try {
        return rp({
          method: 'POST',
          headers: getApiHeaders(headers),
          url: `${process.env.API_URL}/folders`,
          json: true,
          body: {
            name: name,
            parent: parent
          }
        })
      } catch (ex) {
        throw new Error(ex.error)
      }
    },
    destroyFolder: async (_, { id }, { headers }) => {
      try {
        return rp({
          method: 'DELETE',
          headers: getApiHeaders(headers),
          url: `${process.env.API_URL}/folders/${id}`,
          json: true
        })
      } catch (ex) {
        throw new Error(ex.error)
      }
    },
    moveSurveyToFolder: async (_, { id, folderId, surveyId }, { headers }) => {
      try {
        let body = {}
        if (folderId) {
          Object.assign(body, {
            folderId: folderId
          })
        }
        if (surveyId) {
          Object.assign(body, {
            surveyId: surveyId
          })
        }
        return rp({
          method: 'PUT',
          headers: getApiHeaders(headers),
          url: `${process.env.API_URL}/folders/moveToFolder/${id}`,
          json: true,
          body: body
        })
      } catch (ex) {
        throw new Error(ex.error)
      }
    }
  }
}
