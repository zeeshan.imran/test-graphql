/* eslint-env jest */
const { graphql } = require('graphql')
const nock = require('nock')
const schema = require('../../schema')

const mockQuestions = [
  {
    id: 'question-1',
    requiredQuestion: true,
    typeOfQuestion: 'choose-one',
    displayOn: 'middle'
  },
  {
    id: 'question-2',
    requiredQuestion: false,
    typeOfQuestion: 'dropdown',
    displayOn: 'middle'
  },
  {
    id: 'question-3',
    requiredQuestion: true,
    typeOfQuestion: 'paired-questions',
    displayOn: 'middle',
    pairs: [
      {
        id: '5ced25dc79a80317a1386b6f',
        leftAttribute: 'Crunchy',
        rightAttribute: 'Moist',
        leftAttributeCounter: 0,
        rightAttributeCounter: 0,
        question: '5ced25dc79a80317a1386b4d'
      },
      {
        id: '5ced25dc79a80317a1386b70',
        leftAttribute: 'Moist',
        rightAttribute: 'Crunchy',
        leftAttributeCounter: 0,
        rightAttributeCounter: 0,
        question: '5ced25dc79a80317a1386b4d'
      },
      {
        id: '5ced25dc79a80317a1386b71',
        leftAttribute: 'Crunchy',
        rightAttribute: 'Grainy',
        leftAttributeCounter: 0,
        rightAttributeCounter: 0,
        question: '5ced25dc79a80317a1386b4d'
      },
      {
        id: '5ced25dc79a80317a1386b72',
        leftAttribute: 'Grainy',
        rightAttribute: 'Crunchy',
        leftAttributeCounter: 0,
        rightAttributeCounter: 0,
        question: '5ced25dc79a80317a1386b4d'
      },
      {
        id: '5ced25dc79a80317a1386b73',
        leftAttribute: 'Moist',
        rightAttribute: 'Grainy',
        leftAttributeCounter: 0,
        rightAttributeCounter: 0,
        question: '5ced25dc79a80317a1386b4d'
      }
    ],
    pairsOptions: {
      minPairs: 2
    }
  }
]

nock(process.env.API_URL)
  .get('/survey/survey-1')
  .reply(200, {
    id: '123',
    name: 'Test name',
    questions: mockQuestions
  })

  .get('/question/question-1')
  .query(true)
  .reply(200, mockQuestions[0])

describe('loginToSurvey resolver', () => {
  it('should resolve "type", "required" and "pairs" field on both survey.productsQuestions and question', async () => {
    const query = /* GraphQL */ `
      query {
        survey(id: "survey-1") {
          id
          name
          productsQuestions {
            id
            type
            required
            pairs {
              id
              leftAttribute 
              rightAttribute 
            }
          }
        }
        question(id: "question-1") {
          id
          type
          required
          pairs {
            id
            leftAttribute 
            rightAttribute
          }
        }
      }
    `

    const result = await graphql(schema, query, {}, {})

    expect(result).toHaveProperty(
      'data.survey.productsQuestions.0.type',
      'choose-one'
    )
    expect(result).toHaveProperty(
      'data.survey.productsQuestions.1.type',
      'dropdown'
    )
    expect(result).toHaveProperty('data.question.type', 'choose-one')

    expect(result).toHaveProperty(
      'data.survey.productsQuestions.0.required',
      true
    )
    expect(result).toHaveProperty(
      'data.survey.productsQuestions.1.required',
      false
    )
    expect(result).toHaveProperty('data.question.required', true)

    expect(result).toHaveProperty('data.survey.productsQuestions.0.pairs', [])
    expect(result).toHaveProperty(
      'data.survey.productsQuestions.2.pairs.length',
      1
    )
  })
})
