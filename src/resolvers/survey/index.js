const rp = require('request-promise')
const compact = require('lodash.compact')
const formatSurvey = require('../../utils/formatSurvey')
const getApiHeaders = require('../../utils/getApiHeaders')
const env = require('../../config/env')

const calcEmailServiceSettings = ({ authorizationType }) => {
  return ['email', 'selected'].includes(authorizationType)
}

const resetSurvey = async id => {
  try {
    await rp({
      method: 'DELETE',
      uri: `${env.STATS_SERVER_API}/survey/${id}`,
      body: {},
      json: true
    })
  } catch (e) {
    // avoid throw exception on developer machine
    if (process.env.NODE_ENV !== 'development') {
      throw e
    }
  }
}

const resetSurveyQuestions = async id => {
  try {
    await rp({
      method: 'DELETE',
      uri: `${env.STATS_SERVER_API}/survey/${id}/questions`,
      body: {},
      json: true
    })
  } catch (e) {
    // avoid throw exception on developer machine
    if (process.env.NODE_ENV !== 'development') {
      throw e
    }
  }
}

const syncSkipFlowSet = (skipFlow, productIdByClientId) => {
  if (skipFlow && skipFlow.type === 'MATCH_PRODUCT') {
    return {
      skipFlow: {
        ...skipFlow,
        rules: skipFlow.rules.map(rule => ({
          ...rule,
          productId: productIdByClientId[rule.productId] || rule.productId
        }))
      }
    }
  }

  return {}
}

const replaceProducts = async (headers, surveyId, products) =>
  rp({
    headers: getApiHeaders(headers),
    method: 'POST',
    uri: `${process.env.API_URL}/product/replaceProducts`,
    body: { surveyId, products },
    json: true
  })

const replaceQuestions = async (headers, questions) =>
  rp({
    headers: getApiHeaders(headers),
    method: 'POST',
    uri: `${process.env.API_URL}/question/replaceQuestions`,
    body: { questions },
    json: true
  })

const buildClientIdCache = replacedProducts =>
  replacedProducts
    .filter(product => product.clientGeneratedId)
    .reduce(
      (acc, { clientGeneratedId, product }) => ({
        ...acc,
        [clientGeneratedId]: product.id
      }),
      {}
    )

const deprecateSurvey = async (headers, surveyId) =>
  rp({
    headers: getApiHeaders(headers),
    method: 'POST',
    uri: `${process.env.API_URL}/survey/deprecate/${surveyId}`,
    json: true
  })

module.exports = {
  Survey: {
    screeners: async (survey, _, { headers }) =>{
      const authHeaders = getApiHeaders(headers)
      
      let isOwner = true

      if (authHeaders && authHeaders.Authorization) {
        const currentUser = await rp({
          method: 'GET',
          headers: authHeaders,
          uri: `${process.env.API_URL}/user/me`,
          json: true
        })
        isOwner = currentUser.organization === survey.owner
      }

      if(!survey.isCompulsorySurveyShownInShare && !isOwner){
        return []
      }

      return rp({
        method: 'GET',
        headers: getApiHeaders(headers),
        uri: `${process.env.API_URL}/survey/${survey.id}/${!isOwner}/screeners`,
        json: true
      })
    },
    
    compulsory: async (survey, _, { headers }) =>{
      const authHeaders = getApiHeaders(headers)
      
      let isOwner = true

      if (authHeaders && authHeaders.Authorization) {
        const currentUser = await rp({
          method: 'GET',
          headers: authHeaders,
          uri: `${process.env.API_URL}/user/me`,
          json: true
        })
        isOwner = currentUser.organization === survey.owner
      }

      if(!survey.isCompulsorySurveyShownInShare && !isOwner){
        return []
      }
      
      return rp({
        method: 'GET',
        headers: getApiHeaders(headers),
        uri: `${process.env.API_URL}/survey/${survey.id}/${!isOwner}/compulsory`,
        json: true
      })
    },
      
    products: async (survey, _, { productLoader }) => {
      const { products } = survey
      const toProduct = async idOrProduct =>
        typeof idOrProduct === 'string'
          ? productLoader.load(idOrProduct)
          : idOrProduct

      return Promise.all((products || []).map(toProduct))
    },
    sharedStatsUsers: async (survey, _, { userLoader }) => {
      const { sharedStatsUsers } = survey

      const toUser = async idOrUser =>
        typeof idOrUser === 'string' ? userLoader.load(idOrUser) : idOrUser

      return Promise.all((sharedStatsUsers || []).map(toUser))
    },
    stats: async (
      survey,
      {
        productIds,
        questionFilters,
        dateFilter,
        sync = false,
        enrollmentId = []
      },
      { headers }
    ) => {
      questionFilters =
        questionFilters.length &&
        questionFilters.map(filter => {
          if (filter.options && filter.options.length) {
            return filter
          }
        })

      const authHeaders = getApiHeaders(headers)
      let isOwner = true

      if (authHeaders && authHeaders.Authorization) {
        const currentUser = await rp({
          method: 'GET',
          headers: authHeaders,
          uri: `${process.env.API_URL}/user/me`,
          json: true
        })
        isOwner = currentUser.organization === survey.owner
      }

      const { jobGroupId, jobIds, resultSet: stats } = await rp({
        method: 'POST',
        uri: `${env.STATS_SERVER_API}/survey/${survey.id}/charts`,
        body: {
          validation: survey.validatedData,
          products: productIds,
          filterQuestions: compact(questionFilters),
          enrollmentId: enrollmentId,
          excludeSkipPairs: survey.excludeSkipPairs,
          dateFilter,
          sync,
          isShared: !isOwner
        },
        json: true
      })

      return {
        ...stats,
        jobGroupId,
        jobIds,
        tabs: (stats.tabs || []).map(tab => ({
          ...tab,
          charts: (tab.charts || []).map(chart => ({
            ...chart,
            loading: true
          }))
        }))
      }
    }
  },
  Query: {
    getStatsResults: async (_root, { jobGroupId, jobIds }) => {
      const jobs = await rp({
        method: 'GET',
        uri: `${
          env.STATS_SERVER_API
        }/groupJobs/${jobGroupId}?jobSlugs=${JSON.stringify(jobIds)}`,
        json: true
      })

      return jobs && jobs.length
        ? jobs.map(job => ({
            status: job.status,
            jobId: job.slug,
            result: job.data
          }))
        : []
    },
    isUniqueNameDuplicated: async (_, { ...input }, { headers }) => {
      const res = await rp({
        method: 'POST',
        headers: getApiHeaders(headers),
        uri: `${process.env.API_URL}/survey/isDuplicate`,
        body: input,
        json: true
      })

      return res
    },
    surveys: async (_, { input }, { headers }) => {
      const query = JSON.stringify(input)

      const res = await rp({
        method: 'GET',
        headers: getApiHeaders(headers),
        uri: `${process.env.API_URL}/survey/getsurveysbyowner?query=${query}`,
        json: true
      })

      return res
    },
    quickAccessSurveys: async (_, __, { headers }) => {
      const res = await rp({
        method: 'GET',
        headers: getApiHeaders(headers),
        uri: `${process.env.API_URL}/survey/getquickaccesssurveys`,
        json: true
      })

      return res
    },
    surveysTotal: async (_, { input }, { headers }) => {
      const query = JSON.stringify(input)

      const res = await rp({
        method: 'GET',
        headers: getApiHeaders(headers),
        uri: `${process.env.API_URL}/survey/count?query=${query}`,
        json: true
      })

      return res
    },
    allSurveys: async () => {
      const res = await rp({
        method: 'GET',
        uri: `${process.env.API_URL}/survey?limit=100`,
        json: true
      })

      return res.map(formatSurvey)
    },
    survey: async (_, { id, checkOrg }, { headers }) => {
      try {
        let url = `${process.env.API_URL}/survey/${id}`
        if (checkOrg) url = `${process.env.API_URL}/surveybyorganization/${id}`

        const res = await rp({
          method: 'GET',
          uri: url,
          headers: checkOrg ? getApiHeaders(headers) : null,
          json: true
        })

        return formatSurvey(res)
      } catch (error) {
        throw new Error(error)
      }
    },
    getDemoSurvey: async (_, { demoName }) => {
      const res = await rp({
        method: 'GET',
        uri: `${process.env.API_URL}/demosurvey/${demoName}`,
        json: true
      })

      return res
    },
    surveysPpt: async (
      _,
      { surveyId, jobGroupId, currentEnv },
      { headers }
    ) => {
      try {
        let url = `${process.env.API_URL}/surveyStatsPpt/${surveyId}`
        const res = await rp({
          method: 'POST',
          body: {
            jobGroupId,
            currentEnv
          },
          headers: getApiHeaders(headers),
          uri: url,
          json: true
        })

        return res
      } catch (error) {
        throw new Error(error)
      }
    },
    compulsorySurveyCountry: async (_, {}, { headers }) => {
      try {
        let url = `${process.env.API_URL}/compulsory-survey-country`
        const authHeaders = getApiHeaders(headers)
        if (authHeaders && authHeaders.Authorization) {
          const res = await rp({
            headers: authHeaders,
            method: 'GET',
            uri: url,
            json: true
          })
          return res
        }
        throw new Error('No compulsory survey found')
      } catch (error) {
        throw new Error(error)
      }
    },
    getQuestionIdWithOrder: async (_, { surveyId }) => {
      const res = await rp({
        method: 'GET',
        uri: `${process.env.API_URL}/getQuestionWithOrder/${surveyId}`,
        json: true
      })

      return res
    }
  },
  Mutation: {
    setOpenedAt: async (_, { id }) => {
      try {
        let url = `${process.env.API_URL}/openedSurvey/${id}`

        await rp({
          method: 'POST',
          uri: url,
          json: true
        })

        return true
      } catch (error) {
        throw new Error(error)
      }
    },
    createSurvey: async (_, { input }, { headers }) => {
      try {
        const { basics, products, questions: questionsInput } = input
        const { recaptcha } = basics

        const replacedProducts = await replaceProducts(headers, null, products)

        const productIds = replacedProducts.map(({ product }) => product.id)
        const createdSurvey = await rp({
          headers: getApiHeaders(headers),
          method: 'POST',
          uri: `${process.env.API_URL}/survey`,
          body: {
            ...basics,
            products: productIds,
            settings: {
              recaptcha,
              emailService: calcEmailServiceSettings(basics)
            }
          },
          json: true
        })

        try {
          const productIdByClientId = buildClientIdCache(replacedProducts)

          const questions = questionsInput.map(question => ({
            ...question,
            survey: createdSurvey.id,
            ...syncSkipFlowSet(question.skipFlow, productIdByClientId)
          }))

          const replacedQuestions = await replaceQuestions(headers, questions)

          return { survey: createdSurvey, replacedProducts, replacedQuestions }
        } catch (ex) {
          // avoid "Unique name error"
          await deprecateSurvey(headers, createdSurvey.id)
          throw ex
        }
      } catch (error) {
        throw new Error(error.error && error.error.code)
      }
    },
    updateSurveyProducts: async (_, { input }, { headers }) => {
      try {
        const { id, products } = input

        const updatedSurvey = await rp({
          headers: getApiHeaders(headers),
          method: 'PATCH',
          uri: `${process.env.API_URL}/survey/${id}`,
          body: {
            products: products
          },
          json: true
        })
        return updatedSurvey
      } catch (error) {
        throw new Error(error.error && error.error.code)
      }
    },
    updateSurvey: async (_, { input }, { headers }) => {
      try {
        const {
          isRemoveDataFromDraft = true,
          basics,
          products,
          questions: questionsInput
        } = input
        const { id, recaptcha, ...rest } = basics

        const replacedProducts = await replaceProducts(headers, id, products)

        const productIds = replacedProducts.map(({ product }) => product.id)
        const updatedSurvey = await rp({
          headers: getApiHeaders(headers),
          method: 'PATCH',
          uri: `${process.env.API_URL}/survey/${id}`,
          body: {
            ...rest,
            products: productIds,
            settings: {
              recaptcha,
              emailService: calcEmailServiceSettings(input)
            }
          },
          json: true
        })

        const productIdByClientId = buildClientIdCache(replacedProducts)

        const questions = questionsInput.map(question => ({
          ...question,
          survey: updatedSurvey.id,
          ...syncSkipFlowSet(question.skipFlow, productIdByClientId)
        }))

        const replacedQuestions = await replaceQuestions(headers, questions)

        if (updatedSurvey.state === 'draft') {
          if (isRemoveDataFromDraft) {
            await resetSurvey(id)
          }
        } else {
          await resetSurveyQuestions(id)
        }

        return { survey: updatedSurvey, replacedProducts, replacedQuestions }
      } catch (error) {
        throw new Error(error.error && error.error.code)
      }
    },
    updateSurveyBasics: async (_, { input }, { headers }) => {
      try {
        const { id, recaptcha, ...rest } = input

        const updatedSurvey = await rp({
          headers: getApiHeaders(headers),
          method: 'PATCH',
          uri: `${process.env.API_URL}/survey/${id}`,
          body: {
            ...rest,
            settings: {
              recaptcha,
              emailService: calcEmailServiceSettings(input)
            }
          },
          json: true
        })
        if (updatedSurvey.state === 'draft') {
          await resetSurvey(id)
        } else {
          await resetSurveyQuestions(id)
        }
        return updatedSurvey
      } catch (error) {
        throw new Error(error.error && error.error.code)
      }
    },
    cloneSurvey: async (_, { id }, { headers }) => {
      try {
        const { survey: surveyId } = await rp({
          headers: getApiHeaders(headers),
          method: 'POST',
          uri: `${process.env.API_URL}/survey/clone/${id}`,
          json: true
        })

        return surveyId
      } catch (error) {
        throw new Error(error.error && error.error.code)
      }
    },
    deprecateSurvey: async (_, { id }, { headers }) => {
      const res = await deprecateSurvey(headers, id)
      return res
    },
    suspendSurvey: async (_, { id }, { headers }) => {
      const res = await rp({
        headers: getApiHeaders(headers),
        method: 'PATCH',
        uri: `${process.env.API_URL}/survey/${id}`,
        body: {
          state: 'suspended'
        },
        json: true
      })
      return res
    },
    resumeSurvey: async (_, { id }, { headers }) => {
      const res = await rp({
        headers: getApiHeaders(headers),
        method: 'PATCH',
        uri: `${process.env.API_URL}/survey/${id}`,
        body: {
          state: 'active'
        },
        json: true
      })
      return res
    },
    getSurveyShares: async (_, { surveyId }, { headers }) => {
      const res = await rp({
        headers: getApiHeaders(headers),
        method: 'GET',
        uri: `${process.env.API_URL}/survey/download/shares/${surveyId}`,
        body: {
          state: 'active'
        },
        json: true
      })
      return res
    },
    getGiftCardShares: async (_, { surveyId }, { headers }) => {
      const res = await rp({
        headers: getApiHeaders(headers),
        method: 'GET',
        uri: `${process.env.API_URL}/survey/download/gift-card-shares/${surveyId}`,
        body: {
          state: 'active'
        },
        json: true
      })
      return res
    },
    getProductIncentives: async (_, { surveyId }, { headers }) => {
      const res = await rp({
        headers: getApiHeaders(headers),
        method: 'GET',
        uri: `${process.env.API_URL}/survey/download/product-incentives/${surveyId}`,
        body: {
          state: 'active'
        },
        json: true
      })
      return res
    },
    getGiftCardIncentives: async (_, { surveyId }, { headers }) => {
      try {
        const res = await rp({
          headers: getApiHeaders(headers),
          method: 'GET',
          uri: `${process.env.API_URL}/survey/download/gift-card-incentives/${surveyId}`,
          body: {
            state: 'active'
          },
          json: true
        })
        return res
      } catch (error) {
        throw new Error(error.error)
      }
    },
    shareSurveyStatsScreen: async (
      _,
      {
        surveyId,
        users,
        isScreenerShownInShare,
        isCompulsorySurveyShownInShare
      },
      { headers }
    ) => {
      try {
        const res = await rp({
          headers: getApiHeaders(headers),
          method: 'POST',
          uri: `${process.env.API_URL}/survey/share/stats`,
          body: {
            surveyId,
            users,
            isScreenerShownInShare,
            isCompulsorySurveyShownInShare
          },
          json: true
        })

        return res
      } catch (error) {
        throw new Error(error)
      }
    },
    sendSurveyReport: async (
      _,
      {
        surveyId,
        userId,
        screener = '',
        dateFilter,
        includeCompulsorySurvey = false,
        reportList
      }
    ) => {
      await rp({
        method: 'POST',
        url: `${env.EXPORTS_API}/api/export`,
        body: {
          surveyId,
          userId,
          dateFilter,
          screenerId: screener,
          includeCompulsorySurvey,
          reportList
        },
        json: true
      })

      return true
    },
    sendSurveyPhotos: async (_, { surveyId, userId }) => {
      await rp({
        method: 'POST',
        url: `${env.EXPORTS_API}/api/photos-export`,
        body: {
          surveyId,
          userId
        },
        json: true
      })

      return true
    },
    getDownloadLink: async (
      _,
      { surveyId, jobGroupId, layout, templateName },
      { headers }
    ) => {
      try {
        let url = `${process.env.API_URL}/survey-pdf/${templateName}/${surveyId}/${jobGroupId}`
        const res = await rp({
          method: 'POST',
          body: {
            layout
          },
          headers: getApiHeaders(headers),
          uri: url,
          json: true
        })

        return res.downloadFile
      } catch (error) {
        throw new Error(error)
      }
    }
  }
}
