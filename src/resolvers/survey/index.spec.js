/* eslint-env jest */
const { graphql } = require('graphql')
const schema = require('../../schema')
const formatSurvey = require('../../utils/formatSurvey')

jest.mock('../../utils/formatSurvey', () =>
  jest.fn(survey => ({
    id: `formattedSurvey-${survey.id}`,
    questions: []
  }))
)

jest.mock('request-promise', () => ({ uri }) => {
  if (uri.includes('survey/')) {
    // single survey request
    return {
      id: 'abc',
      questions: []
    }
  }
  return [{ id: 'abc', questions: [] }, { id: 'def', questions: [] }]
})

describe('survey resolvers', () => {
  beforeEach(() => {
    formatSurvey.mockClear()
  })
  it('should format fetched survey', async () => {
    const query = /* GraphQL */ `
      query {
        survey(id: "abc") {
          id
        }
      }
    `

    const result = await graphql(schema, query, {}, {})

    expect(formatSurvey).toBeCalled()
    expect(result).toEqual({
      data: { survey: { id: 'formattedSurvey-abc' } }
    })
  })

  it('should format each fetched survey', async () => {
    const query = /* GraphQL */ `
      query {
        allSurveys {
          id
        }
      }
    `

    const result = await graphql(schema, query, {}, {})
    expect(formatSurvey).toBeCalledTimes(2)
    expect(result).toEqual({
      data: {
        allSurveys: [
          { id: 'formattedSurvey-abc' },
          { id: 'formattedSurvey-def' }
        ]
      }
    })
  })
})
