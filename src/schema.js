const { makeExecutableSchema } = require('graphql-tools')
const merge = require('lodash.merge')

const Scalar = require('./types/Scalar')
const Survey = require('./types/Survey')
const Question = require('./types/Question')
const Organization = require('./types/Organization')
const Answer = require('./types/Answer')
const Product = require('./types/Product')
const User = require('./types/User')
const SurveyEnrollment = require('./types/SurveyEnrollment')
const ImageUpload = require('./types/ImageUpload')
const Brand = require('./types/Brand')
const QrCode = require('./types/QrCode')
const File = require('./types/File')
const Settings = require('./types/Settings')
const SurveyAnalysis = require('./types/SurveyAnalysis')
const Folder = require('./types/Folder')
const Logs = require('./types/Logs')
const CountrySurveyGrading = require('./types/CountrySurveyGrading')
const Feedback = require('./types/Feedback')

const SurveyResolvers = require('./resolvers/survey')
const QuestionResolvers = require('./resolvers/question')
const OrganizationResolvers = require('./resolvers/organization')
const AnswerResolvers = require('./resolvers/answer')
const UserResolvers = require('./resolvers/user')
const ImageUploadResolvers = require('./resolvers/imageUpload')
const SurveyEnrollmentResolvers = require('./resolvers/surveyEnrollment')
const ScalarResolvers = require('./resolvers/scalar')
const BrandResolvers = require('./resolvers/brand')
const ProductResolvers = require('./resolvers/product')
const QrCodeResolvers = require('./resolvers/qrCode')
const FileResolvers = require('./resolvers/file')
const SettingsResolvers = require('./resolvers/settings')
const SurveyAnalysisResolvers = require('./resolvers/surveyAnalysis')
const FolderResolvers = require('./resolvers/folder')
const LogsResolvers = require('./resolvers/logs')
const CountrySurveyGradingResolvers = require('./resolvers/countrySurveyGrading')
const FeedbackResolver = require('./resolvers/feedback')

const Root = /* GraphQL */ `
  # The dummy queries and mutations are necessary because
  # graphql-js cannot have empty root types and we only extend
  # these types later on
  # Ref: apollographql/graphql-tools#293
  scalar Object
  type Query {
    dummy: String
  }
  type Mutation {
    dummy: String
  }
  type Subscription {
    dummy: String
  }
  enum OrderDirection {
    ascend
    descend
  }
  schema {
    query: Query
    mutation: Mutation
    subscription: Subscription
  }
`

const RootResolvers = {
  OrderDirection: {
    ascend: 'ASC',
    descend: 'DESC'
  }
}

const resolvers = merge(
  RootResolvers,
  ScalarResolvers,
  SurveyResolvers,
  QuestionResolvers,
  UserResolvers,
  ImageUploadResolvers,
  AnswerResolvers,
  SurveyEnrollmentResolvers,
  BrandResolvers,
  ProductResolvers,
  OrganizationResolvers,
  QrCodeResolvers,
  FileResolvers,
  SettingsResolvers,
  SurveyAnalysisResolvers,
  FolderResolvers,
  LogsResolvers,
  CountrySurveyGradingResolvers,
  FeedbackResolver
)

module.exports = makeExecutableSchema({
  typeDefs: [
    Root,
    Scalar,
    Survey,
    Question,
    Product,
    User,
    SurveyEnrollment,
    ImageUpload,
    Answer,
    Brand,
    Organization,
    QrCode,
    File,
    Settings,
    SurveyAnalysis,
    Folder,
    Logs,
    CountrySurveyGrading,
    Feedback
  ],
  resolvers
})
