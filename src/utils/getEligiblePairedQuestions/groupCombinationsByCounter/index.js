const groupCombinationsByCounter = combinations =>
  Object.keys(combinations).reduce((combinationsByCount, combinationId) => {
    const { counter } = combinations[combinationId]
    const combinationsInCount = [
      ...(combinationsByCount[counter] || []),
      combinationId
    ]

    combinationsByCount[counter] = combinationsInCount
    return combinationsByCount
  }, [])

module.exports = groupCombinationsByCounter
