const { flatten, dropLast } = require('ramda')
const groupCombinationsByCounter = require('./groupCombinationsByCounter')

const getEligiblePairedQuestions = (combinations, minPairs, maxPairs) => {
  const combinationsGroupedByCounter = groupCombinationsByCounter(combinations)
  const combinationsHaveTheSameCounter =
    combinationsGroupedByCounter.length === 1

  if (combinationsHaveTheSameCounter) {
    return flatten(combinationsGroupedByCounter)
  }

  const leastCountedCombinations = flatten(
    dropLast(1, combinationsGroupedByCounter).filter(a => !!a)
  )

  if (leastCountedCombinations.length >= minPairs) {
    return leastCountedCombinations
  } else {
    const mostCountedCombinations = combinationsGroupedByCounter.length ? 
      combinationsGroupedByCounter[combinationsGroupedByCounter.length - 1] : []

    return [
      ...leastCountedCombinations,
      ...mostCountedCombinations.length ? mostCountedCombinations.slice(
        0,
        minPairs - leastCountedCombinations.length
      ) : []
    ]
  }
}

module.exports = getEligiblePairedQuestions
