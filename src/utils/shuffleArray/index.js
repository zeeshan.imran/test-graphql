const shuffleArray = array => {
  let tempArray = array
  let currentIndex = tempArray.length

  let temporaryValue

  let randomIndex

  // While there remain elements to shuffle...
  while (currentIndex !== 0) {
    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex)
    currentIndex -= 1

    // And swap it with the current element.
    temporaryValue = tempArray[currentIndex]
    tempArray[currentIndex] = tempArray[randomIndex]
    tempArray[randomIndex] = temporaryValue
  }

  return tempArray
}

module.exports = shuffleArray
