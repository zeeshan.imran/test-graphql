/* eslint-env jest */
const generatePairedQuestions = require('.')

const mockedCombinations = {
  'A-B': { permutations: ['AB', 'BA'] },
  'A-C': { permutations: ['AC', 'CA'] },
  'B-C': { permutations: ['BC', 'CB'] }
}

const mockedEligiblePairedQuestions = ['A-B', 'B-C']

jest.mock('./getCombinationsWithCounter', () => jest.fn(a => a))
jest.mock('../shuffleArray', () => jest.fn(a => a))
jest.mock('../getEligiblePairedQuestions', () =>
  jest.fn(() => mockedEligiblePairedQuestions)
)
jest.mock('../getRandomElementFromArray', () => jest.fn(a => a[0]))

it('should correctly generate the paired questions', () => {
  expect(generatePairedQuestions(mockedCombinations)).toEqual(['AB', 'BC'])
})
