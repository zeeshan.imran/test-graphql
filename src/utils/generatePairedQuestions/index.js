const getEligiblePairedQuestions = require('../getEligiblePairedQuestions')
const getRandomElementFromArray = require('../getRandomElementFromArray')
const getCombinationsWithCounter = require('./getCombinationsWithCounter')
const shuffleArray = require('../shuffleArray')

const generatePairedQuestions = (pairs, minPairs, maxPairs) => {
  const combinations = getCombinationsWithCounter(pairs)

  const eligibleIndexes = getEligiblePairedQuestions(combinations, minPairs, maxPairs)
  const shuffledIndexes = shuffleArray(eligibleIndexes)
  return shuffledIndexes.map(index =>
    getRandomElementFromArray(combinations[index].permutations)
  )
}

module.exports = generatePairedQuestions
