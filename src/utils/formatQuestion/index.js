const { omit } = require('ramda')
const generatePairedQuestions = require('../generatePairedQuestions')

function formatQuestion (q) {
  const formattedQuestion = omit(['typeOfQuestion', 'requiredQuestion'], q)

  const type = q.typeOfQuestion
  const required = q.requiredQuestion

  if (type === 'paired-questions') {
    const pairs = generatePairedQuestions(q.pairs, q.pairsOptions.minPairs, q.pairsOptions.maxPairs)
    return { ...formattedQuestion, type, required, pairs }
  }

  return { ...formattedQuestion, type, required }
}

module.exports = formatQuestion
