/* eslint-env jest */
const formatQuestion = require('.')

const mockApiQuestion = {
  typeOfQuestion: 'typeABC',
  requiredQuestion: false,
  displayOn: 'public',
  someOtherProp: 'untouched'
}

it('should format an API question to one compliant with the GraphQL schema', () => {
  expect(formatQuestion(mockApiQuestion)).toMatchSnapshot()
})
