/* eslint-env jest */
const getRandomElementFromArray = require('.')

global.Math.random = () => 0

const mockArray = ['A-B', 'A-C', 'B-C']

it('should get the element from the array', () => {
  expect(getRandomElementFromArray(mockArray)).toEqual('A-B')
})
