const { map, find } = require('ramda')
const rp = require('request-promise')
const getApiHeaders = require('../utils/getApiHeaders')

const getUserByIds = async (headers, ids) => {
  const query = JSON.stringify({
    id: ids
  })

  const user = await rp({
    method: 'GET',
    headers: getApiHeaders(headers),
    uri: `${process.env.API_URL}/user?where=${query}`,
    json: true
  })

  return map(id => find(o => o.id === id, user), ids)
}

module.exports = getUserByIds
