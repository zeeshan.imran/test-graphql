const { map, find } = require('ramda')
const rp = require('request-promise')
const getApiHeaders = require('../utils/getApiHeaders')

const getQuestionByIds = async (headers, ids) => {
  const questions = await rp({
    method: 'POST',
    headers: getApiHeaders(headers),
    uri: `${process.env.API_URL}/question/findids`,
    json: true,
    body: {
      ids
    }
  })

  return map(id => find(o => o.id === id, questions), ids)
}

module.exports = getQuestionByIds
