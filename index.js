/* eslint-disable no-console */
const { GraphQLServer } = require('graphql-yoga')
const schema = require('./src/schema')
const createLoaders = require('./src/loaders')
const graphqlOptions = require('./src/config/graphql')
const env = require('./src/config/env')
const compression = require('compression')

const server = new GraphQLServer({
  schema,
  context: context => {
    const headers = context.request.headers
    const loaders = createLoaders({ headers })
    return { headers, ...loaders }
  }
})

server.express.use(compression())

server.start(graphqlOptions, ({ port }) => {
  console.log('--- Flavorwiki GraphQL Server ---')
  console.log(`Server mode : ${process.env.NODE_ENV}`)
  console.log(`API_URL     : ${process.env.API_URL}`)
  console.log(`STATS_API   : ${env.STATS_API} | ${env.STATS_SERVER_API}`)
  console.log(`EXPORTS_API : ${env.EXPORTS_API}`)
  console.log(`Server is running on :${port}`)
})
