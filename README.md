![](https://git.flavorwiki.info/app/graphql/badges/develop/pipeline.svg)
![](https://git.flavorwiki.info/app/graphql/badges/develop/coverage.svg)


### Getting started

`docker-compose build`

`docker-compose run --rm --service-ports api bash`

`npm start` or `npm run dev`