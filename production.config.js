function generateDeploy (branch, envValue) {
  const { SSH_USER, SSH_HOST, DEPLOY_PATH, ENV_PATH } = process.env
  return {
    user: SSH_USER,
    host: SSH_HOST,
    path: DEPLOY_PATH,
    ref: `origin/${branch}`,
    repo: 'git@git.flavorwiki.info:app/graphql.git',
    'pre-deploy': `git fetch && git reset --hard origin/${branch}`,
    'post-deploy': [
      `ln -sf ${ENV_PATH}/graphql.env .env`,
      'npm install',
      `pm2 reload production.config.js --env ${envValue}`
    ].join(' && ')
  }
}

module.exports = {
  apps: [
    {
      name: 'fw-graphql',
      env: {
        NODE_ENV: 'production'
      },
      script: 'index.js',
      exec_mode: 'cluster',
      instances: 2
    }
  ],
  deploy: {
    'master': generateDeploy('master', 'master'),
    'hansen-master': generateDeploy('hansen-master', 'hansen-master'),
    'blc-master': generateDeploy('blc-master', 'hansen-master')
  }
}
